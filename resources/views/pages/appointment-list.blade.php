@extends('design.mainlayout')

@section('content')

<div class="container">
	<table class="table table-hover">
		<thead>
			<th>Appointment Date</th>
			<th>Appointment Time</th>
			<th>Doctor Name</th>
			<th>Status</th>
		</thead>
		<tbody>
			@if($data->isEmpty())
			<div class="alert alert-danger text-center">No Data Found</div>
			@else
			@foreach($data as $dt)
			<tr>
				<td>{{$dt->appointment_date}}</td>
				<td>{{$dt->appointment_time}}</td>
				<td>{{$dt->fullname}}</td>
				@if($dt->status == 1)
				<td >Scheduled</td>
				@elseif($dt->status == 2)
				<td style="color: green;">Done</td>
				@else
				<td style="color: blue;">Reschedule</td>
				@endif
			</tr>
			@endforeach
			@endif


		</tbody>
	</table>
</div>



@endsection
