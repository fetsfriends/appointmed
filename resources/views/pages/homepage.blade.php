@extends('design.mainlayout')

@section('content')

<!--Start rev slider wrapper-->
<section class="rev_slider_wrapper">
  <div id="slider1" class="rev_slider"  data-version="5.0">
    <ul>
      <li data-transition="rs-20">
        <img src="images/slides/1.jpg"  alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

        <div class="tp-caption  tp-resizeme"
        data-x="left" data-hoffset="0"
        data-y="top" data-voffset="220"
        data-transform_idle="o:1;"
        data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
        data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
        data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
        data-splitin="none"
        data-splitout="none"
        data-responsive_offset="on"
        data-start="1500">
        {{-- <div class="slide-content-box mar-lft">
          <h1>Easy<br> Appointment</h1>
          <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
          <div class="button">
            <a class="#" href="#">Read More</a>
            <a class="btn-style-two" href="#">Departments of Hearts</a>
          </div>
        </div> --}}
      </div>

    </li>
    <li data-transition="fade">
      <img src="images/slides/2.jpg" alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

      <div class="tp-caption  tp-resizeme"
      data-x="right" data-hoffset="0"
      data-y="top" data-voffset="220"
      data-transform_idle="o:1;"
      data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
      data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
      data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
      data-splitin="none"
      data-splitout="none"
      data-responsive_offset="on"
      data-start="1500">
      {{-- <div class="slide-content-box">
        <h1>Easy finding<br> doctor <span>For you.</span></h1>
        <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
        <div class="button">
          <a class="#" href="#">Read More</a>
          <a class="btn-style-two" href="#">Departments of kidney</a>
        </div>
      </div> --}}
    </div>

  </li>
  <li data-transition="fade">
    <img src="images/slides/3.jpg"  alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

    <div class="tp-caption  tp-resizeme"
    data-x="left" data-hoffset="0"
    data-y="top" data-voffset="220"
    data-transform_idle="o:1;"
    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
    data-splitin="none"
    data-splitout="none"
    data-responsive_offset="on"
    data-start="1500">
    {{-- <div class="slide-content-box mar-lft">
      <h1>Appointmed providing total<br> healthcare <span>Solutions.</span></h1>
      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
      <div class="button">
        <a class="#" href="#">Read More</a>
        <a class="btn-style-two" href="#">Appointmed Company</a>
      </div>
    </div> --}}
  </div>

</li>
</ul>
</div>
</section>
<!--End rev slider wrapper-->

<!--Start call to action area-->
<section class="callto-action-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner-content">
          <div class="title-box text-center">
            <span class="flaticon-calendar"></span>
            <h2>Make an Appointment</h2>
          </div>
          <div class="form-holder clearfix">
            <form id="appointment" class="clearfix" name="appointment-form" action="{{url('/search')}}" method="get">
              <div class="single-box mar-right-30">
                <div class="input-box">
                  <input type="text" name="condition" value="" placeholder="Condition" >
                </div>
                <div class="input-box">
                  <input type="text" name="doctor" value="" placeholder="Doctor Name or Hospital Name" >
                </div>
                <div class="single-box">
                  <div class="form-group">
                    <select class="form-control" name="health_card" style="height: 50px;" placeholder="Pick Health Card Association">
                      <option value="">Health Card</option>
                      <option value="ac">Asalus Corporation</option>
                      <option value="ahsi">Asiancare Health SYstems, lnc.</option>
                      <option value="amci">Avega Managed Care, lnc.</option>
                      <option value="cplsii">Carehealth Plus Systems lnternational, lnc.</option>
                      <option value="chsi">Carewell Health Systems, lnc.</option>
                      <option value="chsi">Caritas Health Shield, lnc.</option>
                      <option value="chmf">Cooperative Health Management Federation</option>
                      <option value="dcc">Dynamic Care CorP.</option>
                      <option value="ehi">Eastwest Healthcare, lnc.</option>
                      <option value="fmi">Fortune Medicare, lnc.</option>
                      <option value="ghsi">Getwell Health SYstems, lnc.</option>
                      <option value="hcdcp">Health Care & Development Corporationof the PhiliPPines</option>
                      <option value="hmi">Health Maintenance, lnc.</option>
                      <option value="imswci">IMS Wealth Care, lnc.</option>
                      <option value="ihci">lnsular Health Care, lnc.</option>
                      <option value="kihi">Kaiser lnternational Healthgroup, lnc.</option>
                      <option value="lhi">Life & Health HMP, lnc.</option>
                      <option value="mhci">Mazan Health Care, lnc.</option>
                      <option value="mhc">Maxicare Healthcare CorP.</option>
                      <option value="mhsi">Medocare Health SYstems, lnc.</option>
                      <option value="mpi">Medicard Philippines, lnc.</option>
                      <option value="mpi">MedicarePlus, lnc.</option>
                      <option value="mhsi">Metrocare Health System, lnc.</option>
                      <option value="omhsi">Optimum Medical and Healthcare Services, lnc.</option>
                      <option value="pchci">Pacific Cross Health Care, lnc.</option>
                      <option value="pci">PhilhealthCare, lnc.</option>
                      <option value="shsi">Stotsenberg Healthcare Sytem lnc.</option>
                      <option value="tmdci">Transnational Medical & Diagnostic Center,lnc.</option>
                      <option value="vchsi">Value Care Health SYstems, lnc.</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="single-box">
                <div class="input-box">
                  <input type="text" name="city" value="" placeholder="City" >
                </div>
              </div>
              <div class="single-box">
                <div class="form-group">
                  <select id="Specialty" name="specialty" class="form-control" style="height: 50px;\">
                    <option value="">Specialization</option>
                    <option value="acupuncturist">Acupuncturist</option>
                    <option value="allergist">Allergist</option>
                    <option value="anesthesiologist">Anesthesiologist</option>
                    <option value="asthma_educator">Asthma Educator</option>
                    <option value="audiologist">Audiologist</option>
                    <option value="bariatric_physician">Bariatric Physician</option>
                    <option value="bioidentical_hormone_doctor">Bioidentical Hormone Doctor</option>
                    <option value="cardiac_electrophysiologist">Cardiac Electrophysiologist</option>
                    <option value="cardiologist">Cardiologist</option>
                    <option value="cardiothoracic_surgeon">Cardiothoracic Surgeon</option>
                    <option value="chiropractor">Chiropractor</option>
                    <option value="colorectal_surgeon">Colorectal Surgeon</option>
                    <option value="cystic_fibrosis_coordinator">Cystic Fibrosis Coordinator</option>
                    <option value="dentist">Dentist</option>
                    <option value="dermatologist">Dermatologist</option>
                    <option value="dietitian">Dietitian</option>
                    <option value="ear_nose_throat_doctor">Ear, Nose &amp; Throat Doctor</option>
                    <option value="emergency_medicine_physician">Emergency Medicine Physician</option>
                    <option value="endocrinologist">Endocrinologist</option>
                    <option value="endodontist">Endodontist</option>
                    <option value="family_physician">Family Physician</option>
                    <option value="gastroenterologist">Gastroenterologist</option>
                    <option value="geneticist">Geneticist</option>
                    <option value="geriatrician">Geriatrician</option>
                    <option value="hand_microsurgery_specialist">Hand &amp; Microsurgery Specialist</option>
                    <option value="hand_surgeon">Hand Surgeon</option>
                    <option value="hand_therapist">Hand Therapist</option>
                    <option value="hematologist">Hematologist</option>
                    <option value="hospice_and_palliative_medicine_specialist">Hospice and Palliative Medicine Specialist</option>
                    <option value="hospitalist">Hospitalist</option>
                    <option value="immunologist">Immunologist</option>
                    <option value="infectious_disease_specialist">Infectious Disease Specialist</option>
                    <option value="integrative_health_medicine_specialist">Integrative Health Medicine Specialist</option>
                    <option value="internist"></option>
                    <option value="medical_ethicist">Medical Ethicist</option>
                    <option value="microbiologist">Microbiologist</option>
                    <option value="midwife">Midwife</option>
                    <option value="naturopathic_doctor">Naturopathic Doctor</option>
                    <option value="nephrologist">Nephrologist</option>
                    <option value="neurologist">Neurologist</option>
                    <option value="neuromusculoskeletal_medicine_specialist">Neuromusculoskeletal Medicine &amp; OMM Specialist</option>
                    <option value="neuropsychiatrist">Neuropsychiatrist</option>
                    <option value="neurosurgeon">Neurosurgeon</option>
                    <option value="nurse_practitioner">Nurse Practitioner</option>
                    <option value="nutritionist">Nutritionist</option>
                    <option value="ob_gyn">OB-GYN</option>
                    <option value="occupational_medicine_specialist">Occupational Medicine Specialist</option>
                    <option value="occupational_therapist">Occupational Therapist</option>
                    <option value="oncologist">Oncologist</option>
                    <option value="ophthalmologist">Ophthalmologist</option>
                    <option value="optometrist">Optometrist</option>
                    <option value="oral_surgeon">Oral Surgeon</option>
                    <option value="orthodontist">Orthodontist</option>
                    <option value="orthopedic_surgeon">Orthopedic Surgeon</option>
                    <option value="pain_management_specialist">Pain Management Specialist</option>
                    <option value="pathologist">Pathologist</option>
                    <option value="pediatric_dentist">Pediatric Dentist</option>
                    <option value="pediatric_emergency_medicine_specialist">Pediatric Emergency Medicine Specialist</option>
                    <option value="pediatrician">Pediatrician</option>
                    <option value="periodontist">Periodontist</option>
                    <option value="physiatrist">Physiatrist</option>
                    <option value="physical_therapist">Physical Therapist</option>
                    <option value="physician_assistant">Physician Assistant</option>
                    <option value="plastic_surgeon">Plastic Surgeon</option>
                    <option value="podiatrist">Podiatrist</option>
                    <option value="preventive_medicine_specialist">Preventive Medicine Specialist</option>
                    <option value="primary_care_doctor">Primary Care Doctor</option>
                    <option value="prosthodontist">Prosthodontist</option>
                    <option value="psychiatrist">Psychiatrist</option>
                    <option value="psychologist">Psychologist</option>
                    <option value="psychosomatic_medicine_specialist">Psychosomatic Medicine Specialist</option>
                    <option value="psychotherapist">Psychotherapist</option>
                    <option value="pulmonologist">Pulmonologist</option>
                    <option value="radiation_oncologist">Radiation Oncologist</option>
                    <option value="radiologist">Radiologist</option>
                    <option value="reproductive_endocrinologist">Reproductive Endocrinologist</option>
                    <option value="respiratory_therapist">Respiratory Therapist</option>
                    <option value="rheumatologist">Rheumatologist</option>
                    <option value="sleep_medicine_specialist">Sleep Medicine Specialist</option>
                    <option value="speech_language_pathologist">Speech-Language Pathologist</option>
                    <option value="sports_medicine_specialist">Sports Medicine Specialist</option>
                    <option value="surgeon">Surgeon</option>
                    <option value="surgical_oncologist">Surgical Oncologist</option>
                    <option value="thoracic_surgeon">Thoracic Surgeon</option>
                    <option value="travel_medicine_specialist">Travel Medicine Specialist</option>
                    <option value="urgent_care_specialist">Urgent Care Specialist</option>
                    <option value="urological_surgeon">Urological Surgeon</option>
                    <option value="urologist">Urologist</option>
                    <option value="vascular_surgeon">Vascular Surgeon</option>
                    <option value="wound_care_pecialist">Wound Care Specialist</option>
                  </select>
                </div>
              </div>
              <button class="thm-btn bgclr-1" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<!--End call to action area-->

<!--Start Medical Departments area-->
<section class="medical-departments-area">
  <div class="container">
    <div class="sec-title">
      <h1>Search doctors by specialization</h1>
      <span class="border"></span>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="medical-departments-carousel">
          <!--Start single item-->
          <div class="single-item text-center">
            <div class="iocn-holder">
              <span class="flaticon-cardiology"></span>
            </div>
            <div class="text-holder">
              <h3>Cardiology</h3>
              <!-- <p>How all this mistaken al idea of denouncing pleasure praisings pain was complete.</p> -->
            </div>
            <a class="readmore" href="#">Read More</a>
          </div>
          <!--End single item-->
          <!--Start single item-->
          <div class="single-item text-center">
            <div class="iocn-holder">
              <span class="flaticon-lungs"></span>
            </div>
            <div class="text-holder">
              <h3>Pulmonology</h3>
              <!-- <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p> -->
            </div>
            <a class="readmore" href="#">Read More</a>
          </div>
          <!--End single item-->
          <!--Start single item-->
          <div class="single-item text-center">
            <div class="iocn-holder">
              <span class="flaticon-vagina"></span>
            </div>
            <div class="text-holder">
              <h3>Gynecology</h3>
              <!-- <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p> -->
            </div>
            <a class="readmore" href="#">Read More</a>
          </div>
          <!--End single item-->
          <!--Start single item-->
          <div class="single-item text-center">
            <div class="iocn-holder">
              <span class="flaticon-neurology"></span>
            </div>
            <div class="text-holder">
              <h3>Neurology</h3>
              <!-- <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p> -->
            </div>
            <a class="readmore" href="#">Read More</a>
          </div>
          <!--End single item-->
        </div>
      </div>
    </div>
  </div>
</section>
<!--End Medical Departments area-->

<!--Start service area-->

<!--End service area-->

<!--Start team area-->
<section class="team-area">
  <div class="container">
    <div class="sec-title">
      <h1>Our Trusted Doctors to help you</h1>
      <span class="border"></span>
    </div>
    <div class="row">
      <!--Start single item-->
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="single-team-member">
          <div class="img-holder">
            <img src="images/team/1.jpg" alt="Awesome Image">
            <div class="overlay-style">
              <div class="box">
                <div class="content">
                  <div class="top">
                    <h3>Marc Parcival</h3>
                    <span>St. Lukes</span>
                  </div>
                  <span class="border"></span>
                  <div class="bottom">
                    <ul>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> 099999999</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> aaaaa@appointmed.com</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-holder">
              <h3>Marc Parcival</h3>
              <span>St. Lukes</span>
            </div>
          </div>
        </div>
      </div>
      <!--End single item-->
      <!--Start single item-->
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="single-team-member">
          <div class="img-holder">
            <img src="images/team/2.jpg" alt="Awesome Image">
            <div class="overlay-style">
              <div class="box">
                <div class="content">
                  <div class="top">
                    <h3>Alen Bailey</h3>
                    <span>St. Lukes</span>
                  </div>
                  <span class="border"></span>
                  <div class="bottom">
                    <ul>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> +321 567 89 0123</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> Bailey@Hospitals.com</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-holder">
              <h3>Marc Parcival</h3>
              <span>St. Lukes</span>
            </div>
          </div>
        </div>
      </div>
      <!--End single item-->
      <!--Start single item-->
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="single-team-member">
          <div class="img-holder">
            <img src="images/team/3.jpg" alt="Awesome Image">
            <div class="overlay-style">
              <div class="box">
                <div class="content">
                  <div class="top">
                    <h3>Basil Andrew</h3>
                    <span>St. Lukes</span>
                  </div>
                  <span class="border"></span>
                  <div class="bottom">
                    <ul>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> +321 567 89 0123</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> Bailey@Hospitals.com</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-holder">
              <h3>Marc Parcival</h3>
              <span>St. Lukes</span>
            </div>
          </div>
        </div>
      </div>
      <!--End single item-->
      <!--Start single item-->
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="single-team-member">
          <div class="img-holder">
            <img src="images/team/4.jpg" alt="Awesome Image">
            <div class="overlay-style">
              <div class="box">
                <div class="content">
                  <div class="top">
                    <h3>Edgar Denzil</h3>
                    <span>St. Lukes</span>
                  </div>
                  <span class="border"></span>
                  <div class="bottom">
                    <ul>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> +321 567 89 0123</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> Bailey@Hospitals.com</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-holder">
              <h3>Edgar Denzil</h3>
              <span>St. Lukes</span>
            </div>
          </div>
        </div>
      </div>
      <!--End single item-->
    </div>
  </div>
</section>
<!--End team area-->

<!--Start fact counter area-->
{{-- <section class="fact-counter-area" style="background-image:url(images/resources/fact-counter-bg.jpg);">
  <div class="container">
    <div class="sec-title text-center">
      <h1>Keep your headup & be patient</h1>
      <p>How all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the<br> system and expound the actual teachings of the great.</p>
    </div>
    <div class="row">
      <!--Start single item-->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ul>
          <li>
            <div class="single-item text-center">
              <div class="icon-holder">
                <span class="flaticon-medical"></span>
              </div>
              <h1><span class="timer" data-from="1" data-to="25" data-speed="5000" data-refresh-interval="50">25</span></h1>
              <h3>Years of Experience</h3>
            </div>
          </li>
          <li>
            <div class="single-item text-center">
              <div class="icon-holder">
                <span class="flaticon-smile"></span>
              </div>
              <h1><span class="timer" data-from="1" data-to="284" data-speed="5000" data-refresh-interval="50">284</span></h1>
              <h3>Well Smiley Faces</h3>
            </div>
          </li>
          <li>
            <div class="single-item text-center">
              <div class="icon-holder">
                <span class="flaticon-medical-1"></span>
              </div>
              <h1><span class="timer" data-from="1" data-to="176" data-speed="5000" data-refresh-interval="50">176</span></h1>
              <h3>Heart Transplant</h3>
            </div>
          </li>
          <li>
            <div class="single-item text-center">
              <div class="icon-holder">
                <span class="flaticon-ribbon"></span>
              </div>
              <h1><span class="timer" data-from="1" data-to="142" data-speed="5000" data-refresh-interval="50">142</span></h1>
              <h3>Awards Holded</h3>
            </div>
          </li>
        </ul>
      </div>
      <!--End single item-->

    </div>
  </div>
</section> --}}
<!--End fact counter area-->

{{-- <!--Start testimonial area-->
  <section class="testimonial-area">
    <div class="container">
      <div class="sec-title mar0auto text-center">
        <h1>What Our Clients Say</h1>
        <span class="border"></span>
      </div>
      <div class="row">
        <!--Start single item-->
        <div class="col-md-12">
          <div class="testimonial-carousel">
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/1.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>Mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.</p>
              </div>
              <div class="name">
                <h3>Rossy Miranda</h3>
                <span>Newyork</span>
              </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/2.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
              </div>
              <div class="name">
                <h3>Peter Lawrence</h3>
                <span>California</span>
              </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/1.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>Mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.</p>
              </div>
              <div class="name">
                <h3>Rossy Miranda</h3>
                <span>Newyork</span>
              </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/2.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
              </div>
              <div class="name">
                <h3>Peter Lawrence</h3>
                <span>California</span>
              </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/1.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>Mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.</p>
              </div>
              <div class="name">
                <h3>Rossy Miranda</h3>
                <span>Newyork</span>
              </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="single-testimonial-item text-center">
              <div class="img-box">
                <div class="img-holder">
                  <img src="images/testimonial/2.png" alt="Awesome Image">
                </div>
                <div class="quote-box">
                  <i class="fa fa-quote-left" aria-hidden="true"></i>
                </div>
              </div>
              <div class="text-holder">
                <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
              </div>
              <div class="name">
                <h3>Peter Lawrence</h3>
                <span>California</span>
              </div>
            </div>
            <!--End single testimonial item-->
          </div>
        </div>
        <!--End single item-->

      </div>
    </div>
  </section>
  <!--End testimonial area-->  --}}

  {{-- <!--Start latest blog area-->
    <section class="latest-blog-area">
      <div class="container">
        <div class="sec-title">
          <h1>Latest news about our company</h1>
          <span class="border"></span>
        </div>
        <div class="row">
          <!--Start single blog item-->
          <div class="col-md-4">
            <div class="single-blog-item">
              <div class="img-holder">
                <img src="images/blog/latest-blog-1.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                  <div class="box">
                    <div class="content">
                      <a href="blog-single.html"><span class="flaticon-plus-symbol"></span></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-holder">
                <a href="blog-single.html">
                  <h3 class="blog-title">How to handle your kids’ from mystery ailments</h3>
                </a>
                <div class="text">
                  <p>The great explorer of the truth, master builder of human happiness one rejects, dislikes, or avoids pleasure itself because it is pleasure.</p>
                </div>
                <ul class="meta-info">
                  <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>February 14, 2017</a></li>
                  <li><a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>21 Comments</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--End single blog item-->
          <!--Start single blog item-->
          <div class="col-md-4">
            <div class="single-blog-item">
              <div class="img-holder">
                <img src="images/blog/latest-blog-2.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                  <div class="box">
                    <div class="content">
                      <a href="blog-single.html"><span class="flaticon-plus-symbol"></span></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-holder">
                <a href="blog-single.html">
                  <h3 class="blog-title">Negative statin stories add to heart health risk</h3>
                </a>
                <div class="text">
                  <p>There anyone who loves or pursues or desires to obtain pains of itself, because it is pain because occasionally circumstances occur.</p>
                </div>
                <ul class="meta-info">
                  <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>January 21, 2017</a></li>
                  <li><a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>18 Comments</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--End single blog item-->
          <!--Start single blog item-->
          <div class="col-md-4">
            <div class="single-blog-item">
              <div class="img-holder">
                <img src="images/blog/latest-blog-3.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                  <div class="box">
                    <div class="content">
                      <a href="blog-single.html"><span class="flaticon-plus-symbol"></span></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-holder">
                <a href="blog-single.html">
                  <h3 class="blog-title">Lung cancer survival rate in England improves</h3>
                </a>
                <div class="text">
                  <p>Which toil and pain can procure him some great pleasure. To take a trivial example, which of us  laborious physical exercise.</p>
                </div>
                <ul class="meta-info">
                  <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>January 15, 2017</a></li>
                  <li><a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>09 Comments</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--End single blog item-->
        </div>
      </div>
    </section>
    <!--End latest blog area--> --}}

    <!--Start facilities Appointment area-->
    {{-- <section class="facilities-appointment-area">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="facilities-content-box">
              <div class="sec-title">
                <h1>Our Facilities</h1>
                <span class="border"></span>
              </div>
              <!--Start facilities carousel-->
              <div class="facilities-carousel">

                <!------Start single facilities item------->
                <div class="single-facilities-item">
                  <div class="row">
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-transport"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>24 Hrs Ambulance</h3>
                          <p>How all this mistaken idea denoucing pleasure and praisings pain was born complete account expound.</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-drink"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Food & Dietary</h3>
                          <p>The Dietitian plans the diet based on the therapeutic needs of the patient, Local specialties, Continental.</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                  </div>
                  <div class="row">
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-avatar"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Special Nurses</h3>
                          <p>Special nurse services can be arranged through Nursing , master of human happiness.</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-church"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Places of Worship</h3>
                          <p>There is a temple of Goddess Krishna mariamman in the hospital premises, a Namaz room & Prayer cell</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                  </div>
                </div>
                <!-------End single facilities item------>

                <!------Start single facilities item------->
                <div class="single-facilities-item">
                  <div class="row">
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-transport"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>24 Hrs Ambulance</h3>
                          <p>How all this mistaken idea denoucing pleasure and praisings pain was born complete account expound.</p>
                        </div>
                      </div>
                    </div>
                    End single item
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-drink"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Food & Dietary</h3>
                          <p>The Dietitian plans the diet based on the therapeutic needs of the patient, Local specialties, Continental.</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                  </div>
                  <div class="row">
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-avatar"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Special Nurses</h3>
                          <p>Special nurse services can be arranged through Nursing , master of human happiness.</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="col-md-6">
                      <div class="single-item">
                        <div class="icon-holder">
                          <div class="icon-box">
                            <div class="icon">
                              <span class="flaticon-church"></span>
                            </div>
                          </div>
                        </div>
                        <div class="text-holder">
                          <h3>Places of Worship</h3>
                          <p>There is a temple of Goddess Krishna mariamman in the hospital premises, a Namaz room & Prayer cell</p>
                        </div>
                      </div>
                    </div>
                    <!--End single item-->
                  </div>
                </div>
                <!-------End single facilities item------>



              </div>
              <!--End facilities carousel-->
            </div>
          </div>
          <div class="col-md-4">
            <div class="appointment">
              <div class="sec-title">
                <h1>Make an Appointment</h1>
                <span class="border"></span>
              </div>
              <form id="appointment-form" name="appointment-form" action="http://steelthemes.com/demo/html/Hospitals/inc/sendmail.php" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-box">
                      <input type="text" name="form_name" value="" placeholder="Your Name" required="">
                    </div>
                    <div class="input-box">
                      <input type="email" name="form_email" value="" placeholder="Your Email" required="">
                    </div>
                    <div class="input-box">
                      <select class="selectmenu">
                        <option selected="selected">Select Department</option>
                        <option>Cardiology</option>
                        <option>Pulmonology</option>
                        <option>Gynecology</option>
                        <option>Neurology</option>
                        <option>Urology</option>
                        <option>Gastrology</option>
                        <option>Pediatrician</option>
                        <option>Laboratory</option>
                      </select>
                    </div>
                    <div class="input-box">
                      <select class="selectmenu">
                        <option selected="selected">Select Doctor</option>
                        <option>Balance Body Mind</option>
                        <option>Physical Activity</option>
                        <option>Support & Motivation</option>
                        <option>Exercise Program</option>
                        <option>Healthy Daily Life</option>
                        <option>First Hand Advice</option>
                      </select>
                    </div>
                    <button class="thm-btn bgclr-1" type="submit">submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section> --}}
    <!--End facilities Appointment area-->

    <!--Start brand area-->
    <section class="brand-area" style="background-image:url(images/awards/awards-bg.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="text-holder">
              <div class="sec-title">
                <h1>You’re in Good Hands</h1>
              </div>
              <div class="text">
                <p>Need to see a doctor immediately? Appointmed has you covered.
                  Simply use the search bar above to find a doctor near you.
                  Get a confirmed appointment within minutes.
                </p>
                <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> -->
              </div>
            </div>
          </div>
          <!-- <div class="col-md-6">
          <div class="awards-holder">
          <div class="sec-title">
          <h1>Clinic Awards</h1>
        </div>
        <div class="row">
        <div class="col-md-6">
        <div class="single-item">
        <a href="#"><img src="images/awards/1.png" alt="Awesome Brand Image"></a>
      </div>
    </div>
    <div class="col-md-6">
    <div class="single-item">
    <a href="#"><img src="images/awards/2.png" alt="Awesome Brand Image"></a>
  </div>
</div>

<div class="col-md-6">
<div class="single-item">
<a href="#"><img src="images/awards/1.png" alt="Awesome Brand Image"></a>
</div>
</div>
<div class="col-md-6">
<div class="single-item">
<a href="#"><img src="images/awards/2.png" alt="Awesome Brand Image"></a>
</div>
</div>
</div>
</div>
</div> -->
</div>
</div>
</section>
<!--End brand area-->

@endsection
