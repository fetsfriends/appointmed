<!DOCTYPE html>
<html lang="en">
<head>
<title>Admin Dashboard</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/matrix-media.css') }}" />
<link href="{{ asset('admin-assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('admin-assets/css/jquery.gritter.css') }}" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

  @include('pages.admin.admin_design.header')
  @include('pages.admin.admin_design.sidebar')
  @yield('content')
  @include('pages.admin.admin_design.footer')

<script src="{{ asset('admin-assets/js/excanvas.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.ui.custom.js') }}"></script> 
<script src="{{ asset('admin-assets/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.flot.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.flot.resize.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.peity.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/fullcalendar.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.dashboard.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.gritter.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.interface.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.chat.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.validate.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.form_validation.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.wizard.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.uniform.js') }}"></script> 
<script src="{{ asset('admin-assets/js/select2.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.popover.js') }}"></script> 
<script src="{{ asset('admin-assets/js/jquery.dataTables.min.js') }}"></script> 
<script src="{{ asset('admin-assets/js/matrix.tables.js') }}"></script> 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
