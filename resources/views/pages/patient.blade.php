@extends('design.mainlayout')

@section('content')





<!--Start rev slider wrapper-->
<section class="rev_slider_wrapper">

</section>
<!--End rev slider wrapper-->

<section class="callto-action-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <div class="title-box text-center">
                        <span class="flaticon-calendar"></span>
                        <h2>Make an Appointment</h2>
                    </div>
                    <div class="form-holder clearfix">
                        <form id="appointment" class="clearfix" name="appointment-form" action="{{url('/search')}}" method="get">
                            <div class="single-box mar-right-30">
                                <div class="input-box">
                                    <input type="text" name="condition" value="" placeholder="Condition" >
                                </div>
                                <div class="input-box">
                                    <input type="text" name="doctor" value="" placeholder="Doctor Name or Hospital Name" >
                                </div>
                                <div class="single-box">
                                <div class="form-group">
                                    <select class="form-control" name="health_card" style="height: 50px;" placeholder="Pick Health Card Association">
                                        <option value="">Health Card</option>
                                        <option value="ac">Asalus Corporation</option>
                                        <option value="ahsi">Asiancare Health SYstems, lnc.</option>
                                        <option value="amci">Avega Managed Care, lnc.</option>
                                        <option value="cplsii">Carehealth Plus Systems lnternational, lnc.</option>
                                        <option value="chsi">Carewell Health Systems, lnc.</option>
                                        <option value="chsi">Caritas Health Shield, lnc.</option>
                                        <option value="chmf">Cooperative Health Management Federation</option>
                                        <option value="dcc">Dynamic Care CorP.</option>
                                        <option value="ehi">Eastwest Healthcare, lnc.</option>
                                        <option value="fmi">Fortune Medicare, lnc.</option>
                                        <option value="ghsi">Getwell Health SYstems, lnc.</option>
                                        <option value="hcdcp">Health Care & Development Corporationof the PhiliPPines</option>
                                        <option value="hmi">Health Maintenance, lnc.</option>
                                        <option value="imswci">IMS Wealth Care, lnc.</option>
                                        <option value="ihci">lnsular Health Care, lnc.</option>
                                        <option value="kihi">Kaiser lnternational Healthgroup, lnc.</option>
                                        <option value="lhi">Life & Health HMP, lnc.</option>
                                        <option value="mhci">Mazan Health Care, lnc.</option>
                                        <option value="mhc">Maxicare Healthcare CorP.</option>
                                        <option value="mhsi">Medocare Health SYstems, lnc.</option>
                                        <option value="mpi">Medicard Philippines, lnc.</option>
                                        <option value="mpi">MedicarePlus, lnc.</option>
                                        <option value="mhsi">Metrocare Health System, lnc.</option>
                                        <option value="omhsi">Optimum Medical and Healthcare Services, lnc.</option>
                                        <option value="pchci">Pacific Cross Health Care, lnc.</option>
                                        <option value="pci">PhilhealthCare, lnc.</option>
                                        <option value="shsi">Stotsenberg Healthcare Sytem lnc.</option>
                                        <option value="tmdci">Transnational Medical & Diagnostic Center,lnc.</option>
                                        <option value="vchsi">Value Care Health SYstems, lnc.</option>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="single-box">
                                <div class="input-box">
                                    <input type="text" name="city" value="" placeholder="City" >
                                </div>
                            </div>
                            <div class="single-box">
                                <div class="form-group">
                                    <select id="Specialty" name="specialty" class="form-control" style="height: 50px;\">
                                        <option value="">Specialization</option>
                                        <option value="acupuncturist">Acupuncturist</option>
                                        <option value="allergist">Allergist</option>
                                        <option value="anesthesiologist">Anesthesiologist</option>
                                        <option value="asthma_educator">Asthma Educator</option>
                                        <option value="audiologist">Audiologist</option>
                                        <option value="bariatric_physician">Bariatric Physician</option>
                                        <option value="bioidentical_hormone_doctor">Bioidentical Hormone Doctor</option>
                                        <option value="cardiac_electrophysiologist">Cardiac Electrophysiologist</option>
                                        <option value="cardiologist">Cardiologist</option>
                                        <option value="cardiothoracic_surgeon">Cardiothoracic Surgeon</option>
                                        <option value="chiropractor">Chiropractor</option>
                                        <option value="colorectal_surgeon">Colorectal Surgeon</option>
                                        <option value="cystic_fibrosis_coordinator">Cystic Fibrosis Coordinator</option>
                                        <option value="dentist">Dentist</option>
                                        <option value="dermatologist">Dermatologist</option>
                                        <option value="dietitian">Dietitian</option>
                                        <option value="ear_nose_throat_doctor">Ear, Nose &amp; Throat Doctor</option>
                                        <option value="emergency_medicine_physician">Emergency Medicine Physician</option>
                                        <option value="endocrinologist">Endocrinologist</option>
                                        <option value="endodontist">Endodontist</option>
                                        <option value="family_physician">Family Physician</option>
                                        <option value="gastroenterologist">Gastroenterologist</option>
                                        <option value="geneticist">Geneticist</option>
                                        <option value="geriatrician">Geriatrician</option>
                                        <option value="hand_microsurgery_specialist">Hand &amp; Microsurgery Specialist</option>
                                        <option value="hand_surgeon">Hand Surgeon</option>
                                        <option value="hand_therapist">Hand Therapist</option>
                                        <option value="hematologist">Hematologist</option>
                                        <option value="hospice_and_palliative_medicine_specialist">Hospice and Palliative Medicine Specialist</option>
                                        <option value="hospitalist">Hospitalist</option>
                                        <option value="immunologist">Immunologist</option>
                                        <option value="infectious_disease_specialist">Infectious Disease Specialist</option>
                                        <option value="integrative_health_medicine_specialist">Integrative Health Medicine Specialist</option>
                                        <option value="internist"></option>
                                        <option value="medical_ethicist">Medical Ethicist</option>
                                        <option value="microbiologist">Microbiologist</option>
                                        <option value="midwife">Midwife</option>
                                        <option value="naturopathic_doctor">Naturopathic Doctor</option>
                                        <option value="nephrologist">Nephrologist</option>
                                        <option value="neurologist">Neurologist</option>
                                        <option value="neuromusculoskeletal_medicine_specialist">Neuromusculoskeletal Medicine &amp; OMM Specialist</option>
                                        <option value="neuropsychiatrist">Neuropsychiatrist</option>
                                        <option value="neurosurgeon">Neurosurgeon</option>
                                        <option value="nurse_practitioner">Nurse Practitioner</option>
                                        <option value="nutritionist">Nutritionist</option>
                                        <option value="ob_gyn">OB-GYN</option>
                                        <option value="occupational_medicine_specialist">Occupational Medicine Specialist</option>
                                        <option value="occupational_therapist">Occupational Therapist</option>
                                        <option value="oncologist">Oncologist</option>
                                        <option value="ophthalmologist">Ophthalmologist</option>
                                        <option value="optometrist">Optometrist</option>
                                        <option value="oral_surgeon">Oral Surgeon</option>
                                        <option value="orthodontist">Orthodontist</option>
                                        <option value="orthopedic_surgeon">Orthopedic Surgeon</option>
                                        <option value="pain_management_specialist">Pain Management Specialist</option>
                                        <option value="pathologist">Pathologist</option>
                                        <option value="pediatric_dentist">Pediatric Dentist</option>
                                        <option value="pediatric_emergency_medicine_specialist">Pediatric Emergency Medicine Specialist</option>
                                        <option value="pediatrician">Pediatrician</option>
                                        <option value="periodontist">Periodontist</option>
                                        <option value="physiatrist">Physiatrist</option>
                                        <option value="physical_therapist">Physical Therapist</option>
                                        <option value="physician_assistant">Physician Assistant</option>
                                        <option value="plastic_surgeon">Plastic Surgeon</option>
                                        <option value="podiatrist">Podiatrist</option>
                                        <option value="preventive_medicine_specialist">Preventive Medicine Specialist</option>
                                        <option value="primary_care_doctor">Primary Care Doctor</option>
                                        <option value="prosthodontist">Prosthodontist</option>
                                        <option value="psychiatrist">Psychiatrist</option>
                                        <option value="psychologist">Psychologist</option>
                                        <option value="psychosomatic_medicine_specialist">Psychosomatic Medicine Specialist</option>
                                        <option value="psychotherapist">Psychotherapist</option>
                                        <option value="pulmonologist">Pulmonologist</option>
                                        <option value="radiation_oncologist">Radiation Oncologist</option>
                                        <option value="radiologist">Radiologist</option>
                                        <option value="reproductive_endocrinologist">Reproductive Endocrinologist</option>
                                        <option value="respiratory_therapist">Respiratory Therapist</option>
                                        <option value="rheumatologist">Rheumatologist</option>
                                        <option value="sleep_medicine_specialist">Sleep Medicine Specialist</option>
                                        <option value="speech_language_pathologist">Speech-Language Pathologist</option>
                                        <option value="sports_medicine_specialist">Sports Medicine Specialist</option>
                                        <option value="surgeon">Surgeon</option>
                                        <option value="surgical_oncologist">Surgical Oncologist</option>
                                        <option value="thoracic_surgeon">Thoracic Surgeon</option>
                                        <option value="travel_medicine_specialist">Travel Medicine Specialist</option>
                                        <option value="urgent_care_specialist">Urgent Care Specialist</option>
                                        <option value="urological_surgeon">Urological Surgeon</option>
                                        <option value="urologist">Urologist</option>
                                        <option value="vascular_surgeon">Vascular Surgeon</option>
                                        <option value="wound_care_pecialist">Wound Care Specialist</option>
                                    </select>
                                </div>
                            </div>
                            <button class="thm-btn bgclr-1" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!--Start Medical Departments area-->
<section class="medical-departments-area">
    <div class="container">
        <div class="sec-title">
            <h1>Medical Departments</h1>
            <span class="border"></span>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="medical-departments-carousel">
                    <!--Start single item-->
                    <div class="single-item text-center">
                        <div class="iocn-holder">
                            <span class="flaticon-cardiology"></span>
                        </div>
                        <div class="text-holder">
                            <h3>Cardiology</h3>
                            <p>How all this mistaken al idea of denouncing pleasure praisings pain was complete.</p>
                        </div>
                        <a class="readmore" href="#">Read More</a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item text-center">
                        <div class="iocn-holder">
                            <span class="flaticon-lungs"></span>
                        </div>
                        <div class="text-holder">
                            <h3>Pulmonology</h3>
                            <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p>
                        </div>
                        <a class="readmore" href="#">Read More</a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item text-center">
                        <div class="iocn-holder">
                            <span class="flaticon-vagina"></span>
                        </div>
                        <div class="text-holder">
                            <h3>Gynecology</h3>
                            <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p>
                        </div>
                        <a class="readmore" href="#">Read More</a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item text-center">
                        <div class="iocn-holder">
                            <span class="flaticon-neurology"></span>
                        </div>
                        <div class="text-holder">
                            <h3>Neurology</h3>
                            <p> Who chooses to enjoy a pleasure that has annoying consquences, or one who avoids a pain.</p>
                        </div>
                        <a class="readmore" href="#">Read More</a>
                    </div>
                    <!--End single item-->
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Medical Departments area-->


@endsection
