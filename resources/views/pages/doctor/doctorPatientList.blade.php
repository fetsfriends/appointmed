@extends('design.doctorlayout.mainlayout')

@section('content')

<div class="wrapper">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </form>
      <div class="header-icon">
        <i class="fa fa-tachometer"></i>
      </div>
      <div class="header-title">
        <h1> Patient List</h1>
        <small> Patient features</small>
        <ol class="breadcrumb hidden-xs">
          <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </div>
    </section>

    <div class="panel-default">
      <div class="panel-heading">
        Patient List
      </div>
      <div class="panel-body">
        <table class="table table-hover">
          <thead>
            <th>Name</th>
            <th>Age</th>
            <th>Contact</th>
            <th>Gender</th>
            <th>Appointment</th>
            <th>Status</th>
            <th class="text-center">Action</th>
          </thead>
          <tbody>
            @if($data->isEmpty())
            <div class="text-center">No Data Found</div>
            @else
            @foreach($data as $dt)
            @if($dt->status == 1)
            <tr>
              <td>{{$dt->name}}</td>
              <td>{{$dt->age}}</td>
              <td>{{$dt->contact}}</td>
              <td>{{$dt->gender}}</td>
              <td>{{$dt->appointment_date.' @ '.$dt->appointment_time}}</td>
              <td>Scheduled</td>
              <td class="text-center"><a href="#" data-ids="{{$dt->aptid}}" class="btn btn-info done">Mark as Done</a> |<a href="#" data-ids="{{$dt->aptid}}" class="btn btn-warning resched">Reschedule</a></td>
            </tr>
            @elseif($dt->status == 2)
            <tr>
              <td>{{$dt->name}}</td>
              <td>{{$dt->age}}</td>
              <td>{{$dt->contact}}</td>
              <td>{{$dt->gender}}</td>
              <td>{{$dt->appointment_date.' @ '.$dt->appointment_time}}</td>
              <td style="color: green;">Done</td>
              <td class="text-center"><a href="#" class="btn btn-info" disabled>Mark as Done</a> |<a href="#" class="btn btn-warning" disabled>Reschedule</a></td>
            </tr>
            @else
            <tr>
              <td>{{$dt->name}}</td>
              <td>{{$dt->age}}</td>
              <td>{{$dt->contact}}</td>
              <td>{{$dt->gender}}</td>
              <td>{{$dt->appointment_date.' @ '.$dt->appointment_time}}</td>
              <td style="color: blue;">Rescheduled</td>
              <td class="text-center"><a href="#" class="btn btn-info" disabled>Mark as Done</a> | <a href="#" class="btn btn-warning" disabled>Reschedule</a></td>
            </tr>
            @endif
            @endforeach
            @endif

          </tbody>
        </table>
      </div>
    </div>

  </div> <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018. All rights reserved.
    </footer>
  </div> <!-- ./wrapper -->
  <!-- ./wrapper -->



  @endsection
