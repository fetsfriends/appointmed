@extends('design.doctorlayout.mainlayout')

@section('content')


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>   
		<div class="header-icon">
			<i class="fa fa-tachometer"></i>
		</div>
		<div class="header-title">
			<h1> Change Password</h1>
			<small> Dashboard features</small>
			{{-- <ol class="breadcrumb hidden-xs">
				<li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol> --}}
		</div>
	</section>
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd lobidrag">
					<div class="panel-body">
						<form class="col-sm-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" name="password" placeholder="Password" required>
							</div>
							<div class="form-group">
								<label>Confirm Passowrd</label>
								<input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required>
							</div>                                  
							<div class="reset-button">
								<a href="#" class="btn btn-success">Update Password</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section> <!-- /.content -->

@endsection