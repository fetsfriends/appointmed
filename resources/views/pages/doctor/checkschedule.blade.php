@extends('design.doctorlayout.mainlayout')

@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <div class="header-icon">
      <i class="fa fa-tachometer"></i>
    </div>
    <div class="header-title">
      <h1> Add Schedule</h1>
      <small> Dashboard features</small>
      <ol class="breadcrumb hidden-xs">
        <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </div>
  </section>
<section class="content">
  <div class="row">
    <form class="" action="{{url('/doctor/store')}}" method="post">
      {{csrf_field()}}
      <input type="hidden" class="sched-id" name="sched_date" value="{{$date}}">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Date</th>
          <th>Time</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
    @foreach($data as $t)
          <tr id="test">
            <input type="hidden" class="sched-id" name="schedule[]" value="{{$t}}">
            <!-- <input type="hidden" class="sched-id" name="schedule['date'][]" value="{{date('F, d o')}}"> -->
            <td>{{$date}}</td>
            <td>{{$t}}</td>
            <td><a class="btn btn-danger test-remove" data-sched="{{$t}}">Remove</a></td>
          </tr>
    @endforeach
  </tbody>
  </table>
  </div>
  <button class="btn btn-info" type="submit">Submit</button>
</form>

</sectiom>
</div>
@endsection
