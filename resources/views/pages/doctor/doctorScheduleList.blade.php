@extends('design.doctorlayout.mainlayout')

@section('content')

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
           <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <div class="header-icon">
            <i class="fa fa-tachometer"></i>
        </div>
        <div class="header-title">
            <h1> Schedule List</h1>
            <small> Schedule features</small>
            <ol class="breadcrumb hidden-xs">
                <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>

	<div class="panel-default">
		<div class="panel-heading">
			Schedule List
		</div>
		<div class="panel-body">
			<table class="table table-hover">
		<thead>
			<th>Schedule</th>
			<th>Status</th>
			<th class="text-center">Action</th>
		</thead>
		<tbody>
      @if($data->isEmpty())
      <div class="text-center">No Data Found</div>
      @else
      @foreach($data as $dt)
      @if($dt->status == 0)
			<tr>
				<td>{{$dt->schedule_date.' @ '.$dt->schedule_time}}</td>
				<td>Available</td>
				<td class="text-center"><a href="/doctor/add-patient/{{$dt->id}}" class="btn btn-info">Appoint</a> | <a href="#" class="btn btn-danger">Remove</a></td>
			</tr>
      @else
      <tr>
        <td>{{$dt->schedule_date.' @ '.$dt->schedule_time}}</td>
        <td>Not Available</td>
        <td class="text-center"><a href="#" class="btn btn-info" disabled>Appoint</a> | <a href="#" class="btn btn-danger" disabled>Remove</a></td>
      </tr>
      @endif
      @endforeach
      @endif
		</tbody>
	</table>
  {{$data->links()}}

		</div>
	</div>





</div> <!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2018. All rights reserved.
    </footer>
</div> <!-- ./wrapper -->
<!-- ./wrapper -->



@endsection
