@extends('design.doctorlayout.mainlayout')
<script>
"use strict"; // Start of use strict
// notification
setTimeout(function () {
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 1000
    };
    toastr.success('{{ Auth::user()->fullname }}', 'Welcome back!');

}, 1300);
</script>
@section('content')
<!-- Site wrapper -->
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
           <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <div class="header-icon">
            <i class="fa fa-tachometer"></i>
        </div>
        <div class="header-title">
            <h1> Dashboard</h1>
            <small> Dashboard features</small>
            <ol class="breadcrumb hidden-xs">
                <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
        <div class="panel panel-bd cardbox">
            <div class="panel-body">
                <div class="statistic-box">
                    <h2><span class="count-number">6</span>
                    </h2>
                </div>
                <div class="items pull-left">
                    <i class="fa fa-user-circle fa-2x"></i>
                    <h4> Total Appoinments</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
        <div class="panel panel-bd cardbox">
            <div class="panel-body">
                <div class="statistic-box">
                    <h2><span class="count-number">3</span>
                    </h2>
                </div>
                <div class="items pull-left">
                    <i class="fa fa-users fa-2x"></i>
                    <h4>Patient History</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
        <div class="panel panel-bd cardbox">
            <div class="panel-body">
                <div class="statistic-box">
                    <h2><span class="count-number">4</span>
                    </h2>
                </div>
                <div class="items pull-left">
                    <i class="fa fa-users fa-2x"></i>
                    <h4>Schedule today</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
        <div class="panel panel-bd cardbox">
            <div class="panel-body">
                <div class="statistic-box">
                    <h2><span class="count-number">7</span>
                    </h2>
                </div>
                <div class="items pull-left">
                    <i class="fa fa-users fa-2x"></i>
                    <h4>Cancelled</h4>
                    <h4>Appointment</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 table-resposive" style="font-size: 15px">
      <div class="panel panel-default">
        <a href="/doctor/schedule-list" class="btn pull-right">View All</a>
        <div class="panel-heading">
          <strong>Schedule List</strong>
      </div>
      <table style="font-size: 15px;" class="table table-hover table-bordered">
          <thead class="text-center">
            <th>Schedule</th>
            <th>Status</th>
        </thead>
        <tbody>
            <tr>
              @if($data_sched->isEmpty())
              <div class="text-center">No Data Found</div>
              @else
              @foreach($data_sched as $dt)
              @if($dt->status == 0)
        			<tr>
        				<td>{{$dt->schedule_date.' @ '.$dt->schedule_time}}</td>
        				<td>Available</td>
        			</tr>
              @else
              <tr>
                <td>{{$dt->schedule_date.' @ '.$dt->schedule_time}}</td>
                <td>Not Available</td>
              </tr>
              @endif
              @endforeach
              @endif
          </tr>
      </tbody>
  </table>
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 table-resposive">
      <div class="panel panel-default">
        <a href="/doctor/patient-list" class="btn pull-right">View All</a>
        <div class="panel-heading">
          <strong>Appointment List</strong>
      </div>
      <table class="table table-hover table-bordered">
          <thead>
            <th>Name</th>
      			<th>Contact</th>
            <th>Appointment</th>
      			<th>Status</th>
        </thead>
        <tbody>
          @if($data_appointment->isEmpty())
          <div class="text-center">No Data Found</div>
          @else
          @foreach($data_appointment as $dta)
          @if($dt->status == 1)
    			<tr>
    				<td>{{$dta->name}}</td>
            <td>{{$dta->contact}}</td>
    				<td>{{$dta->appointment_date.' @ '.$dta->appointment_time}}</td>
            <td>Scheduled</td>
    			</tr>
          @elseif($dta->status == 2)
          <tr>
            <td>{{$dta->name}}</td>
            <td>{{$dta->contact}}</td>
            <td>{{$dta->appointment_date.' @ '.$dta->appointment_time}}</td>
            <td style="color: green;">Done</td>
          </tr>
          @else
          <tr>
            <td>{{$dta->name}}</td>
            <td>{{$dta->contact}}</td>
            <td>{{$dta->appointment_date.' @ '.$dta->appointment_time}}</td>
            <td style="color: blue;">Rescheduled</td>
          </tr>
          @endif
          @endforeach
          @endif

      </tbody>
  </table>
</div>
</div>
</section> <!-- /.content -->

</div> <!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2018. All rights reserved.
    </footer>
</div> <!-- ./wrapper -->
<!-- ./wrapper -->

@endsection
