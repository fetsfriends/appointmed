@extends('design.doctorlayout.mainlayout')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<div class="header-icon">
			<i class="fa fa-tachometer"></i>
		</div>
		<div class="header-title">
			<h1> Add Walk-in Patient</h1>
			<small> Patient features</small>
			<ol class="breadcrumb hidden-xs">
				<li><a href="#"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd lobidrag">
					<div class="panel-body">
						<form class="col-sm-6" action="{{url('/doctor/add-patient/')}}" method="post">
							@if (session('status'))
									<div class="alert alert-success">
											{{ session('status') }}
									</div>
							@endif
							{{csrf_field()}}
              <div class="form-group">
                <label>Schedule</label>
                <input type="text" name="datetime" class="form-control" value="{{$data->schedule_date.' @ '.$data->schedule_time}}" disabled required>
                <input type="hidden" name="date" value="{{$data->schedule_date}}">
                <input type="hidden" name="time" value="{{$data->schedule_time}}">
                <input type="hidden" name="id" value="{{$data->id}}">
              </div>
							<div class="form-group">
								<label>Full Name</label>
								@if ($errors->has('fullname'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('fullname') }}</strong>
								</div>
								@endif
								<input type="text" name="fullname" class="form-control" placeholder="Full Name" value="" required>
							</div>

							<div class="form-group">
								<label >Contact</label>
								@if ($errors->has('contact'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('contact') }}</strong>
								</div>
								@endif
								<input type="text" class="form-control" name="contact" placeholder="Contact" value="" required>
							</div>
                <button type="submit" class="btn btn-info" >Submit</button>

@endsection
