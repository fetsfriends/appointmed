@extends('design.doctorlayout.mainlayout')

@section('content')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<div class="header-icon">
			<i class="fa fa-tachometer"></i>
		</div>
		<div class="header-title">
			<h1> Edit Doctors</h1>
			<small> Dashboard features</small>
			<ol class="breadcrumb hidden-xs">
				<li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd lobidrag">
					<div class="panel-body">
						<form action="{{url('/doctor/edit-doctor').'/'.Auth::user()->id}}" id="editform" method="post" enctype="multipart/form-data">
							@if (session('status'))
							<div class="alert alert-success">
								{{ session('status') }}
							</div>
							@endif
							{{csrf_field()}}
							{{method_field('put')}}
							<div class="col-sm-6">
								<div class="form-group">
									<label>Full Name</label>
									@if ($errors->has('fullname'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('fullname') }}</strong>
									</div>
									@endif
									<input type="text" name="fullname" class="form-control" placeholder="Full Name" value="{{Auth::user()->fullname}}" required>
								</div>
								<div class="form-group">
									<label >Email</label>
									@if ($errors->has('email'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('email') }}</strong>
									</div>
									@endif
									<input type="email" class="form-control" name="email" placeholder="Email" value="{{Auth::user()->email}}" required>
								</div>
								<div class="form-group">
									<label >Contact</label>
									@if ($errors->has('contact'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('contact') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control" name="contact" placeholder="Contact" value="{{Auth::user()->contact}}" required>
								</div>
								<div class="form-group">
									<label >Write-up</label>
									@if ($errors->has('description'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('description') }}</strong>
									</div>
									@endif
									<textarea class="form-control" name="description" placeholder="Description" reqiored>{{Auth::user()->description}}</textarea>
								</div>
								<div class="form-group">
									<label>Specialization</label>
									@if ($errors->has('specialization'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('specialization') }}</strong>
									</div>
									@endif
									<select id="Specialty" name="specialization" class="form-control" required>
										<option value="">Specialization</option>
										<option value="acupuncturist" @if(Auth::user()->specialization == 'acupuncturist') {  selected="selected" } @endif>Acupuncturist</option>
										<option value="allergist" @if(Auth::user()->specialization == 'allergist') {  selected="selected" } @endif>Allergist</option>
										<option value="anesthesiologist"@if(Auth::user()->specialization == 'anesthesiologist') {  selected="selected" } @endif>Anesthesiologist</option>
										<option value="asthma_educator"@if(Auth::user()->specialization == 'asthma_educator') {  selected="selected" } @endif>Asthma Educator</option>
										<option value="audiologist"@if(Auth::user()->specialization == 'audiologist') {  selected="selected" } @endif>Audiologist</option>
										<option value="bariatric_physician"@if(Auth::user()->specialization == 'bariatric_physician') {  selected="selected" } @endif>Bariatric Physician</option>
										<option value="bioidentical_hormone_doctor"@if(Auth::user()->specialization == 'bioidentical_hormone_doctor') {  selected="selected" } @endif>Bioidentical Hormone Doctor</option>
										<option value="cardiac_electrophysiologist"@if(Auth::user()->specialization == 'cardiac_electrophysiologist') {  selected="selected" } @endif>Cardiac Electrophysiologist</option>
										<option value="cardiologist"@if(Auth::user()->specialization == 'cardiothoracic_surgeon') {  selected="selected" } @endif>Cardiologist</option>
										<option value="cardiothoracic_surgeon"@if(Auth::user()->specialization == 'cardiothoracic_surgeon') {  selected="selected" } @endif>Cardiothoracic Surgeon</option>
										<option value="chiropractor"@if(Auth::user()->specialization == 'chiropractor') {  selected="selected" } @endif>Chiropractor</option>
										<option value="colorectal_surgeon"@if(Auth::user()->specialization == 'colorectal_surgeon') {  selected="selected" } @endif>Colorectal Surgeon</option>
										<option value="cystic_fibrosis_coordinator"@if(Auth::user()->specialization == 'cystic_fibrosis_coordinator') {  selected="selected" } @endif>Cystic Fibrosis Coordinator</option>
										<option value="dentist" @if(Auth::user()->specialization == 'dentist') {  selected="selected" } @endif>Dentist</option>
										<option value="dermatologist"@if(Auth::user()->specialization == 'dermatologist') {  selected="selected" } @endif>Dermatologist</option>
										<option value="dietitian"@if(Auth::user()->specialization == 'dietitian') {  selected="selected" } @endif>Dietitian</option>
										<option value="ear_nose_throat_doctor"@if(Auth::user()->specialization == 'ear_nose_throat_doctor') {  selected="selected" } @endif>Ear, Nose &amp; Throat Doctor</option>
										<option value="emergency_medicine_physician"@if(Auth::user()->specialization == 'emergency_medicine_physician') {  selected="selected" } @endif>Emergency Medicine Physician</option>
										<option value="endocrinologist"@if(Auth::user()->specialization == 'endocrinologist') {  selected="selected" } @endif>Endocrinologist</option>
										<option value="endodontist"@if(Auth::user()->specialization == 'endodontist') {  selected="selected" } @endif>Endodontist</option>
										<option value="family_physician"@if(Auth::user()->specialization == 'family_physician') {  selected="selected" } @endif>Family Physician</option>
										<option value="gastroenterologist"@if(Auth::user()->specialization == 'gastroenterologist') {  selected="selected" } @endif>Gastroenterologist</option>
										<option value="geneticist"@if(Auth::user()->specialization == 'geneticist') {  selected="selected" } @endif>Geneticist</option>
										<option value="geriatrician"@if(Auth::user()->specialization == 'geriatrician') {  selected="selected" } @endif>Geriatrician</option>
										<option value="hand_microsurgery_specialist"@if(Auth::user()->specialization == 'hand_microsurgery_specialist') {  selected="selected" } @endif>Hand &amp; Microsurgery Specialist</option>
										<option value="hand_surgeon"@if(Auth::user()->specialization == 'hand_surgeon') {  selected="selected" } @endif>Hand Surgeon</option>
										<option value="hand_therapist"@if(Auth::user()->specialization == 'hand_therapist') {  selected="selected" } @endif>Hand Therapist</option>
										<option value="hematologist"@if(Auth::user()->specialization == 'hematologist') {  selected="selected" } @endif>Hematologist</option>
										<option value="hospice_and_palliative_medicine_specialist"@if(Auth::user()->specialization == 'hospice_and_palliative_medicine_specialist') {  selected="selected" } @endif>Hospice and Palliative Medicine Specialist</option>
										<option value="hospitalist"@if(Auth::user()->specialization == 'hospitalist') {  selected="selected" } @endif>Hospitalist</option>
										<option value="immunologist"@if(Auth::user()->specialization == 'immunologist') {  selected="selected" } @endif>Immunologist</option>
										<option value="infectious_disease_specialist"@if(Auth::user()->specialization == 'infectious_disease_specialist') {  selected="selected" } @endif>Infectious Disease Specialist</option>
										<option value="integrative_health_medicine_specialist"@if(Auth::user()->specialization == 'integrative_health_medicine_specialist') {  selected="selected" } @endif>Integrative Health Medicine Specialist</option>
										<option value="internist"@if(Auth::user()->specialization == 'internist') {  selected="selected" } @endif></option>
										<option value="medical_ethicist"@if(Auth::user()->specialization == 'medical_ethicist') {  selected="selected" } @endif>Medical Ethicist</option>
										<option value="microbiologist"@if(Auth::user()->specialization == 'microbiologist') {  selected="selected" } @endif>Microbiologist</option>
										<option value="midwife"@if(Auth::user()->specialization == 'midwife') {  selected="selected" } @endif>Midwife</option>
										<option value="naturopathic_doctor"@if(Auth::user()->specialization == 'naturopathic_doctor') {  selected="selected" } @endif>Naturopathic Doctor</option>
										<option value="nephrologist"@if(Auth::user()->specialization == 'nephrologist') {  selected="selected" } @endif>Nephrologist</option>
										<option value="neurologist"@if(Auth::user()->specialization == 'neurologist') {  selected="selected" } @endif>Neurologist</option>
										<option value="neuromusculoskeletal_medicine_specialist"@if(Auth::user()->specialization == 'neuromusculoskeletal_medicine_specialist') {  selected="selected" } @endif>Neuromusculoskeletal Medicine &amp; OMM Specialist</option>
										<option value="neuropsychiatrist"@if(Auth::user()->specialization == 'neuropsychiatrist') {  selected="selected" } @endif>Neuropsychiatrist</option>
										<option value="neurosurgeon"@if(Auth::user()->specialization == 'neurosurgeon') {  selected="selected" } @endif>Neurosurgeon</option>
										<option value="nurse_practitioner"@if(Auth::user()->specialization == 'nurse_practitioner') {  selected="selected" } @endif>Nurse Practitioner</option>
										<option value="nutritionist"@if(Auth::user()->specialization == 'nutritionist') {  selected="selected" } @endif>Nutritionist</option>
										<option value="ob_gyn"@if(Auth::user()->specialization == 'ob_gyn') {  selected="selected" } @endif>OB-GYN</option>
										<option value="occupational_medicine_specialist"@if(Auth::user()->specialization == 'occupational_medicine_specialist') {  selected="selected" } @endif>Occupational Medicine Specialist</option>
										<option value="occupational_therapist"@if(Auth::user()->specialization == 'occupational_therapist') {  selected="selected" } @endif>Occupational Therapist</option>
										<option value="oncologist"@if(Auth::user()->specialization == 'oncologist') {  selected="selected" } @endif>Oncologist</option>
										<option value="ophthalmologist"@if(Auth::user()->specialization == 'ophthalmologist') {  selected="selected" } @endif>Ophthalmologist</option>
										<option value="optometrist"@if(Auth::user()->specialization == 'optometrist') {  selected="selected" } @endif>Optometrist</option>
										<option value="oral_surgeon"@if(Auth::user()->specialization == 'oral_surgeon') {  selected="selected" } @endif>Oral Surgeon</option>
										<option value="orthodontist"@if(Auth::user()->specialization == 'orthodontist') {  selected="selected" } @endif>Orthodontist</option>
										<option value="orthopedic_surgeon"@if(Auth::user()->specialization == 'orthopedic_surgeon') {  selected="selected" } @endif>Orthopedic Surgeon</option>
										<option value="pain_management_specialist"@if(Auth::user()->specialization == 'pain_management_specialist') {  selected="selected" } @endif>Pain Management Specialist</option>
										<option value="pathologist"@if(Auth::user()->specialization == 'pathologist') {  selected="selected" } @endif>Pathologist</option>
										<option value="pediatric_dentist"@if(Auth::user()->specialization == 'pediatric_dentist') {  selected="selected" } @endif>Pediatric Dentist</option>
										<option value="pediatric_emergency_medicine_specialist"@if(Auth::user()->specialization == 'pediatric_emergency_medicine_specialist') {  selected="selected" } @endif>Pediatric Emergency Medicine Specialist</option>
										<option value="pediatrician"@if(Auth::user()->specialization == 'pediatrician') {  selected="selected" } @endif>Pediatrician</option>
										<option value="periodontist"@if(Auth::user()->specialization == 'periodontist') {  selected="selected" } @endif>Periodontist</option>
										<option value="physiatrist"@if(Auth::user()->specialization == 'physiatrist') {  selected="selected" } @endif>Physiatrist</option>
										<option value="physical_therapist"@if(Auth::user()->specialization == 'physical_therapist') {  selected="selected" } @endif>Physical Therapist</option>
										<option value="physician_assistant"@if(Auth::user()->specialization == 'physician_assistant') {  selected="selected" } @endif>Physician Assistant</option>
										<option value="plastic_surgeon"@if(Auth::user()->specialization == 'plastic_surgeon') {  selected="selected" } @endif>Plastic Surgeon</option>
										<option value="podiatrist"@if(Auth::user()->specialization == 'podiatrist') {  selected="selected" } @endif>Podiatrist</option>
										<option value="preventive_medicine_specialist"@if(Auth::user()->specialization == 'preventive_medicine_specialist') {  selected="selected" } @endif>Preventive Medicine Specialist</option>
										<option value="primary_care_doctor"@if(Auth::user()->specialization == 'primary_care_doctor') {  selected="selected" } @endif>Primary Care Doctor</option>
										<option value="prosthodontist"@if(Auth::user()->specialization == 'prosthodontist') {  selected="selected" } @endif>Prosthodontist</option>
										<option value="psychiatrist"@if(Auth::user()->specialization == 'psychiatrist') {  selected="selected" } @endif>Psychiatrist</option>
										<option value="psychologist"@if(Auth::user()->specialization == 'psychologist') {  selected="selected" } @endif>Psychologist</option>
										<option value="psychosomatic_medicine_specialist"@if(Auth::user()->specialization == 'psychosomatic_medicine_specialist') {  selected="selected" } @endif>Psychosomatic Medicine Specialist</option>
										<option value="psychotherapist"@if(Auth::user()->specialization == 'psychotherapist') {  selected="selected" } @endif>Psychotherapist</option>
										<option value="pulmonologist"@if(Auth::user()->specialization == 'pulmonologist') {  selected="selected" } @endif>Pulmonologist</option>
										<option value="radiation_oncologist"@if(Auth::user()->specialization == 'radiation_oncologist') {  selected="selected" } @endif>Radiation Oncologist</option>
										<option value="radiologist"@if(Auth::user()->specialization == 'radiologist') {  selected="selected" } @endif>Radiologist</option>
										<option value="reproductive_endocrinologist"@if(Auth::user()->specialization == 'reproductive_endocrinologist') {  selected="selected" } @endif>Reproductive Endocrinologist</option>
										<option value="respiratory_therapist"@if(Auth::user()->specialization == 'respiratory_therapist') {  selected="selected" } @endif>Respiratory Therapist</option>
										<option value="rheumatologist"@if(Auth::user()->specialization == 'rheumatologist') {  selected="selected" } @endif>Rheumatologist</option>
										<option value="sleep_medicine_specialist"@if(Auth::user()->specialization == 'sleep_medicine_specialist') {  selected="selected" } @endif>Sleep Medicine Specialist</option>
										<option value="speech_language_pathologist"@if(Auth::user()->specialization == 'speech_language_pathologist') {  selected="selected" } @endif>Speech-Language Pathologist</option>
										<option value="sports_medicine_specialist"@if(Auth::user()->specialization == 'sports_medicine_specialist') {  selected="selected" } @endif>Sports Medicine Specialist</option>
										<option value="surgeon"@if(Auth::user()->specialization == 'surgeon') {  selected="selected" } @endif>Surgeon</option>
										<option value="surgical_oncologist"@if(Auth::user()->specialization == 'surgical_oncologist') {  selected="selected" } @endif>Surgical Oncologist</option>
										<option value="thoracic_surgeon"@if(Auth::user()->specialization == 'thoracic_surgeon') {  selected="selected" } @endif>Thoracic Surgeon</option>
										<option value="travel_medicine_specialist"@if(Auth::user()->specialization == 'travel_medicine_specialist') {  selected="selected" } @endif>Travel Medicine Specialist</option>
										<option value="urgent_care_specialist"@if(Auth::user()->specialization == 'urgent_care_specialist') {  selected="selected" } @endif>Urgent Care Specialist</option>
										<option value="urological_surgeon"@if(Auth::user()->specialization == 'urological_surgeon') {  selected="selected" } @endif>Urological Surgeon</option>
										<option value="urologist"@if(Auth::user()->specialization == 'urologist') {  selected="selected" } @endif>Urologist</option>
										<option value="vascular_surgeon"@if(Auth::user()->specialization == 'vascular_surgeon') {  selected="selected" } @endif>Vascular Surgeon</option>
										<option value="wound_care_pecialist"@if(Auth::user()->specialization == 'wound_care_pecialist') {  selected="selected" } @endif>Wound Care Specialist</option>

									</select>
								</div>
								<div class="form-group">
									<label>Health Card Affiliation:</label> <a href="#" class="append pull-right"><i class="fa fa-plus-square" style="font-size: 20px;"aria-hidden="true"></i></a>
									@if ($errors->has('healthcard'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('healthcard') }}</strong>
									</div>
									@endif
									<?php
									$test = explode(',',Auth::user()->healthcard);
									?>
									@foreach($test as $t)
									<select class="form-control" style="margin-bottom:15px;" name="healthcard[]" placeholder="Pick Health Card Affiliation" >
										<option value="">Health Card</option>
										<option value="ac"@if($t == 'ac') {  selected="selected" } @endif>Asalus Corporation</option>
										<option value="ahsi"@if($t == 'ahsi') {  selected="selected" } @endif>Asiancare Health SYstems, lnc.</option>
										<option value="amci"@if($t == 'amci') {  selected="selected" } @endif>Avega Managed Care, lnc.</option>
										<option value="cplsii"@if($t == 'cplsii') {  selected="selected" } @endif>Carehealth Plus Systems lnternational, lnc.</option>
										<option value="chsi"@if($t == 'chsi') {  selected="selected" } @endif>Carewell Health SYstems, lnc.</option>
										<option value="chsinc"@if($t == 'chsinc') {  selected="selected" } @endif>Caritas Health Shield, lnc.</option>
										<option value="chmf"@if($t == 'chmf') {  selected="selected" } @endif>Cooperative Health Management Federation</option>
										<option value="dcc"@if($t == 'dcc') {  selected="selected" } @endif>Dynamic Care CorP.</option>
										<option value="ehi"@if($t == 'ehi') {  selected="selected" } @endif>Eastwest Healthcare, lnc.</option>
										<option value="fmi"@if($t == 'fmi') {  selected="selected" } @endif>Fortune Medicare, lnc.</option>
										<option value="ghsi"@if($t == 'ghsi') {  selected="selected" } @endif>Getwell Health SYstems, lnc.</option>
										<option value="hcdcp"@if($t == 'hcdcp') {  selected="selected" } @endif>Health Care & Development Corporationof the PhiliPPines</option>
										<option value="hmi"@if($t == 'hmi') {  selected="selected" } @endif>Health Maintenance, lnc.</option>
										<option value="imswci"@if($t == 'imswci') {  selected="selected" } @endif>IMS Wellth Care, lnc.</option>
										<option value="ihci"@if($t == 'ihci') {  selected="selected" } @endif>lnsular Health Care, lnc.</option>
										<option value="kihi"@if($t == 'kihi') {  selected="selected" } @endif>Kaiser lnternational Healthgroup, lnc.</option>
										<option value="lhi"@if($t == 'lhi') {  selected="selected" } @endif>Life & Health HMP, lnc.</option>
										<option value="mhci"@if($t == 'mhci') {  selected="selected" } @endif>Mazan Health Care, lnc.</option>
										<option value="mhc"@if($t == 'mhc') {  selected="selected" } @endif>Maxicare Healthcare CorP.</option>
										<option value="mhsi"@if($t == 'mhsi') {  selected="selected" } @endif>Medocare Health Systems, lnc.</option>
										<option value="mpi"@if($t == 'mpi') {  selected="selected" } @endif>Medicard PhiliPPines, lnc.</option>
										<option value="mpinc"@if($t == 'mpinc') {  selected="selected" } @endif>MedicarePlus, lnc.</option>
										<option value="mhsinc"@if($t == 'mhsinc') {  selected="selected" } @endif>Metrocare Health System, lnc.</option>
										<option value="omhsi"@if($t == 'omhsi') {  selected="selected" } @endif>Optimum Medical and Healthcare Services, lnc.</option>
										<option value="pchci"@if($t == 'pchci') {  selected="selected" } @endif>Pacific Cross Health Care, lnc.</option>
										<option value="pci"@if($t == 'pci') {  selected="selected" } @endif>PhilhealthCare, lnc.</option>
										<option value="shsi"@if($t == 'shsi') {  selected="selected" } @endif>Stotsenberg Healthcare Sytem lnc.</option>
										<option value="tmdci"@if($t == 'tmdci') {  selected="selected" } @endif>Transnational Medical & Diagnostic Center,lnc.</option>
										<option value="vchsi"@if($t == 'vchsi') {  selected="selected" } @endif>Value Care Health SYstems, lnc.</option>
									</select>
									@endforeach
										<div id="append"></div>
								</div>

								<div class="form-group">
									<label>Upload Image</label>
									@if ($errors->has('image_name'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('image_name') }}</strong>
									</div>
									@endif
									<input type="file" name="image_name" class="form-control">
								</div>
							</div>

							<div class="col-sm-6">

								<div class="form-group">
									<label>Sub-Specialization </label> (optional)
									@if ($errors->has('sub_specialization'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('sub_specialization') }}</strong>
									</div>
									@endif
									<select id="Specialty" name="sub_specialization" class="form-control">
										<option value="">Sub-Specialization</option>
										<option value="acupuncturist" @if(Auth::user()->sub_specialization == 'acupuncturist') {  selected="selected" } @endif>Acupuncturist</option>
										<option value="allergist" @if(Auth::user()->sub_specialization == 'allergist') {  selected="selected" } @endif>Allergist</option>
										<option value="anesthesiologist"@if(Auth::user()->sub_specialization == 'anesthesiologist') {  selected="selected" } @endif>Anesthesiologist</option>
										<option value="asthma_educator"@if(Auth::user()->sub_specialization == 'asthma_educator') {  selected="selected" } @endif>Asthma Educator</option>
										<option value="audiologist"@if(Auth::user()->sub_specialization == 'audiologist') {  selected="selected" } @endif>Audiologist</option>
										<option value="bariatric_physician"@if(Auth::user()->sub_specialization == 'bariatric_physician') {  selected="selected" } @endif>Bariatric Physician</option>
										<option value="bioidentical_hormone_doctor"@if(Auth::user()->sub_specialization == 'bioidentical_hormone_doctor') {  selected="selected" } @endif>Bioidentical Hormone Doctor</option>
										<option value="cardiac_electrophysiologist"@if(Auth::user()->sub_specialization == 'cardiac_electrophysiologist') {  selected="selected" } @endif>Cardiac Electrophysiologist</option>
										<option value="cardiologist"@if(Auth::user()->sub_specialization == 'cardiothoracic_surgeon') {  selected="selected" } @endif>Cardiologist</option>
										<option value="cardiothoracic_surgeon"@if(Auth::user()->sub_specialization == 'cardiothoracic_surgeon') {  selected="selected" } @endif>Cardiothoracic Surgeon</option>
										<option value="chiropractor"@if(Auth::user()->sub_specialization == 'chiropractor') {  selected="selected" } @endif>Chiropractor</option>
										<option value="colorectal_surgeon"@if(Auth::user()->sub_specialization == 'colorectal_surgeon') {  selected="selected" } @endif>Colorectal Surgeon</option>
										<option value="cystic_fibrosis_coordinator"@if(Auth::user()->sub_specialization == 'cystic_fibrosis_coordinator') {  selected="selected" } @endif>Cystic Fibrosis Coordinator</option>
										<option value="dentist" @if(Auth::user()->sub_specialization == 'dentist') {  selected="selected" } @endif>Dentist</option>
										<option value="dermatologist"@if(Auth::user()->sub_specialization == 'dermatologist') {  selected="selected" } @endif>Dermatologist</option>
										<option value="dietitian"@if(Auth::user()->sub_specialization == 'dietitian') {  selected="selected" } @endif>Dietitian</option>
										<option value="ear_nose_throat_doctor"@if(Auth::user()->sub_specialization == 'ear_nose_throat_doctor') {  selected="selected" } @endif>Ear, Nose &amp; Throat Doctor</option>
										<option value="emergency_medicine_physician"@if(Auth::user()->sub_specialization == 'emergency_medicine_physician') {  selected="selected" } @endif>Emergency Medicine Physician</option>
										<option value="endocrinologist"@if(Auth::user()->sub_specialization == 'endocrinologist') {  selected="selected" } @endif>Endocrinologist</option>
										<option value="endodontist"@if(Auth::user()->sub_specialization == 'endodontist') {  selected="selected" } @endif>Endodontist</option>
										<option value="family_physician"@if(Auth::user()->sub_specialization == 'family_physician') {  selected="selected" } @endif>Family Physician</option>
										<option value="gastroenterologist"@if(Auth::user()->sub_specialization == 'gastroenterologist') {  selected="selected" } @endif>Gastroenterologist</option>
										<option value="geneticist"@if(Auth::user()->sub_specialization == 'geneticist') {  selected="selected" } @endif>Geneticist</option>
										<option value="geriatrician"@if(Auth::user()->sub_specialization == 'geriatrician') {  selected="selected" } @endif>Geriatrician</option>
										<option value="hand_microsurgery_specialist"@if(Auth::user()->sub_specialization == 'hand_microsurgery_specialist') {  selected="selected" } @endif>Hand &amp; Microsurgery Specialist</option>
										<option value="hand_surgeon"@if(Auth::user()->sub_specialization == 'hand_surgeon') {  selected="selected" } @endif>Hand Surgeon</option>
										<option value="hand_therapist"@if(Auth::user()->sub_specialization == 'hand_therapist') {  selected="selected" } @endif>Hand Therapist</option>
										<option value="hematologist"@if(Auth::user()->sub_specialization == 'hematologist') {  selected="selected" } @endif>Hematologist</option>
										<option value="hospice_and_palliative_medicine_specialist"@if(Auth::user()->sub_specialization == 'hospice_and_palliative_medicine_specialist') {  selected="selected" } @endif>Hospice and Palliative Medicine Specialist</option>
										<option value="hospitalist"@if(Auth::user()->sub_specialization == 'hospitalist') {  selected="selected" } @endif>Hospitalist</option>
										<option value="immunologist"@if(Auth::user()->sub_specialization == 'immunologist') {  selected="selected" } @endif>Immunologist</option>
										<option value="infectious_disease_specialist"@if(Auth::user()->sub_specialization == 'infectious_disease_specialist') {  selected="selected" } @endif>Infectious Disease Specialist</option>
										<option value="integrative_health_medicine_specialist"@if(Auth::user()->sub_specialization == 'integrative_health_medicine_specialist') {  selected="selected" } @endif>Integrative Health Medicine Specialist</option>
										<option value="internist"@if(Auth::user()->sub_specialization == 'internist') {  selected="selected" } @endif></option>
										<option value="medical_ethicist"@if(Auth::user()->sub_specialization == 'medical_ethicist') {  selected="selected" } @endif>Medical Ethicist</option>
										<option value="microbiologist"@if(Auth::user()->sub_specialization == 'microbiologist') {  selected="selected" } @endif>Microbiologist</option>
										<option value="midwife"@if(Auth::user()->sub_specialization == 'midwife') {  selected="selected" } @endif>Midwife</option>
										<option value="naturopathic_doctor"@if(Auth::user()->sub_specialization == 'naturopathic_doctor') {  selected="selected" } @endif>Naturopathic Doctor</option>
										<option value="nephrologist"@if(Auth::user()->sub_specialization == 'nephrologist') {  selected="selected" } @endif>Nephrologist</option>
										<option value="neurologist"@if(Auth::user()->sub_specialization == 'neurologist') {  selected="selected" } @endif>Neurologist</option>
										<option value="neuromusculoskeletal_medicine_specialist"@if(Auth::user()->sub_specialization == 'neuromusculoskeletal_medicine_specialist') {  selected="selected" } @endif>Neuromusculoskeletal Medicine &amp; OMM Specialist</option>
										<option value="neuropsychiatrist"@if(Auth::user()->sub_specialization == 'neuropsychiatrist') {  selected="selected" } @endif>Neuropsychiatrist</option>
										<option value="neurosurgeon"@if(Auth::user()->sub_specialization == 'neurosurgeon') {  selected="selected" } @endif>Neurosurgeon</option>
										<option value="nurse_practitioner"@if(Auth::user()->sub_specialization == 'nurse_practitioner') {  selected="selected" } @endif>Nurse Practitioner</option>
										<option value="nutritionist"@if(Auth::user()->sub_specialization == 'nutritionist') {  selected="selected" } @endif>Nutritionist</option>
										<option value="ob_gyn"@if(Auth::user()->sub_specialization == 'ob_gyn') {  selected="selected" } @endif>OB-GYN</option>
										<option value="occupational_medicine_specialist"@if(Auth::user()->sub_specialization == 'occupational_medicine_specialist') {  selected="selected" } @endif>Occupational Medicine Specialist</option>
										<option value="occupational_therapist"@if(Auth::user()->sub_specialization == 'occupational_therapist') {  selected="selected" } @endif>Occupational Therapist</option>
										<option value="oncologist"@if(Auth::user()->sub_specialization == 'oncologist') {  selected="selected" } @endif>Oncologist</option>
										<option value="ophthalmologist"@if(Auth::user()->sub_specialization == 'ophthalmologist') {  selected="selected" } @endif>Ophthalmologist</option>
										<option value="optometrist"@if(Auth::user()->sub_specialization == 'optometrist') {  selected="selected" } @endif>Optometrist</option>
										<option value="oral_surgeon"@if(Auth::user()->sub_specialization == 'oral_surgeon') {  selected="selected" } @endif>Oral Surgeon</option>
										<option value="orthodontist"@if(Auth::user()->sub_specialization == 'orthodontist') {  selected="selected" } @endif>Orthodontist</option>
										<option value="orthopedic_surgeon"@if(Auth::user()->sub_specialization == 'orthopedic_surgeon') {  selected="selected" } @endif>Orthopedic Surgeon</option>
										<option value="pain_management_specialist"@if(Auth::user()->sub_specialization == 'pain_management_specialist') {  selected="selected" } @endif>Pain Management Specialist</option>
										<option value="pathologist"@if(Auth::user()->sub_specialization == 'pathologist') {  selected="selected" } @endif>Pathologist</option>
										<option value="pediatric_dentist"@if(Auth::user()->sub_specialization == 'pediatric_dentist') {  selected="selected" } @endif>Pediatric Dentist</option>
										<option value="pediatric_emergency_medicine_specialist"@if(Auth::user()->sub_specialization == 'pediatric_emergency_medicine_specialist') {  selected="selected" } @endif>Pediatric Emergency Medicine Specialist</option>
										<option value="pediatrician"@if(Auth::user()->sub_specialization == 'pediatrician') {  selected="selected" } @endif>Pediatrician</option>
										<option value="periodontist"@if(Auth::user()->sub_specialization == 'periodontist') {  selected="selected" } @endif>Periodontist</option>
										<option value="physiatrist"@if(Auth::user()->sub_specialization == 'physiatrist') {  selected="selected" } @endif>Physiatrist</option>
										<option value="physical_therapist"@if(Auth::user()->sub_specialization == 'physical_therapist') {  selected="selected" } @endif>Physical Therapist</option>
										<option value="physician_assistant"@if(Auth::user()->sub_specialization == 'physician_assistant') {  selected="selected" } @endif>Physician Assistant</option>
										<option value="plastic_surgeon"@if(Auth::user()->sub_specialization == 'plastic_surgeon') {  selected="selected" } @endif>Plastic Surgeon</option>
										<option value="podiatrist"@if(Auth::user()->sub_specialization == 'podiatrist') {  selected="selected" } @endif>Podiatrist</option>
										<option value="preventive_medicine_specialist"@if(Auth::user()->sub_specialization == 'preventive_medicine_specialist') {  selected="selected" } @endif>Preventive Medicine Specialist</option>
										<option value="primary_care_doctor"@if(Auth::user()->sub_specialization == 'primary_care_doctor') {  selected="selected" } @endif>Primary Care Doctor</option>
										<option value="prosthodontist"@if(Auth::user()->sub_specialization == 'prosthodontist') {  selected="selected" } @endif>Prosthodontist</option>
										<option value="psychiatrist"@if(Auth::user()->sub_specialization == 'psychiatrist') {  selected="selected" } @endif>Psychiatrist</option>
										<option value="psychologist"@if(Auth::user()->sub_specialization == 'psychologist') {  selected="selected" } @endif>Psychologist</option>
										<option value="psychosomatic_medicine_specialist"@if(Auth::user()->sub_specialization == 'psychosomatic_medicine_specialist') {  selected="selected" } @endif>Psychosomatic Medicine Specialist</option>
										<option value="psychotherapist"@if(Auth::user()->sub_specialization == 'psychotherapist') {  selected="selected" } @endif>Psychotherapist</option>
										<option value="pulmonologist"@if(Auth::user()->sub_specialization == 'pulmonologist') {  selected="selected" } @endif>Pulmonologist</option>
										<option value="radiation_oncologist"@if(Auth::user()->sub_specialization == 'radiation_oncologist') {  selected="selected" } @endif>Radiation Oncologist</option>
										<option value="radiologist"@if(Auth::user()->sub_specialization == 'radiologist') {  selected="selected" } @endif>Radiologist</option>
										<option value="reproductive_endocrinologist"@if(Auth::user()->sub_specialization == 'reproductive_endocrinologist') {  selected="selected" } @endif>Reproductive Endocrinologist</option>
										<option value="respiratory_therapist"@if(Auth::user()->sub_specialization == 'respiratory_therapist') {  selected="selected" } @endif>Respiratory Therapist</option>
										<option value="rheumatologist"@if(Auth::user()->sub_specialization == 'rheumatologist') {  selected="selected" } @endif>Rheumatologist</option>
										<option value="sleep_medicine_specialist"@if(Auth::user()->sub_specialization == 'sleep_medicine_specialist') {  selected="selected" } @endif>Sleep Medicine Specialist</option>
										<option value="speech_language_pathologist"@if(Auth::user()->sub_specialization == 'speech_language_pathologist') {  selected="selected" } @endif>Speech-Language Pathologist</option>
										<option value="sports_medicine_specialist"@if(Auth::user()->sub_specialization == 'sports_medicine_specialist') {  selected="selected" } @endif>Sports Medicine Specialist</option>
										<option value="surgeon"@if(Auth::user()->sub_specialization == 'surgeon') {  selected="selected" } @endif>Surgeon</option>
										<option value="surgical_oncologist"@if(Auth::user()->sub_specialization == 'surgical_oncologist') {  selected="selected" } @endif>Surgical Oncologist</option>
										<option value="thoracic_surgeon"@if(Auth::user()->sub_specialization == 'thoracic_surgeon') {  selected="selected" } @endif>Thoracic Surgeon</option>
										<option value="travel_medicine_specialist"@if(Auth::user()->sub_specialization == 'travel_medicine_specialist') {  selected="selected" } @endif>Travel Medicine Specialist</option>
										<option value="urgent_care_specialist"@if(Auth::user()->sub_specialization == 'urgent_care_specialist') {  selected="selected" } @endif>Urgent Care Specialist</option>
										<option value="urological_surgeon"@if(Auth::user()->sub_specialization == 'urological_surgeon') {  selected="selected" } @endif>Urological Surgeon</option>
										<option value="urologist"@if(Auth::user()->sub_specialization == 'urologist') {  selected="selected" } @endif>Urologist</option>
										<option value="vascular_surgeon"@if(Auth::user()->sub_specialization == 'vascular_surgeon') {  selected="selected" } @endif>Vascular Surgeon</option>
										<option value="wound_care_pecialist"@if(Auth::user()->sub_specialization == 'wound_care_pecialist') {  selected="selected" } @endif>Wound Care Specialist</option>
									</select>
								</div>

								<!-- <div class="form-group">
									<label>Health Card Affiliation:</label>
									@if ($errors->has('healthcard'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('healthcard') }}</strong>
									</div>
									@endif
									<select class="form-control" name="healthcard" placeholder="Pick Health Card Affiliation" >
										<option>Health Card</option>
										<option value="ac"@if(Auth::user()->healthcard == 'ac') {  selected="selected" } @endif>Asalus Corporation</option>
										<option value="ahsi"@if(Auth::user()->healthcard == 'ahsi') {  selected="selected" } @endif>Asiancare Health SYstems, lnc.</option>
										<option value="amci"@if(Auth::user()->healthcard == 'amci') {  selected="selected" } @endif>Avega Managed Care, lnc.</option>
										<option value="cplsii"@if(Auth::user()->healthcard == 'cplsii') {  selected="selected" } @endif>Carehealth Plus Systems lnternational, lnc.</option>
										<option value="chsi"@if(Auth::user()->healthcard == 'chsi') {  selected="selected" } @endif>Carewell Health SYstems, lnc.</option>
										<option value="chsinc"@if(Auth::user()->healthcard == 'chsinc') {  selected="selected" } @endif>Caritas Health Shield, lnc.</option>
										<option value="chmf"@if(Auth::user()->healthcard == 'chmf') {  selected="selected" } @endif>Cooperative Health Management Federation</option>
										<option value="dcc"@if(Auth::user()->healthcard == 'dcc') {  selected="selected" } @endif>Dynamic Care CorP.</option>
										<option value="ehi"@if(Auth::user()->healthcard == 'ehi') {  selected="selected" } @endif>Eastwest Healthcare, lnc.</option>
										<option value="fmi"@if(Auth::user()->healthcard == 'fmi') {  selected="selected" } @endif>Fortune Medicare, lnc.</option>
										<option value="ghsi"@if(Auth::user()->healthcard == 'ghsi') {  selected="selected" } @endif>Getwell Health SYstems, lnc.</option>
										<option value="hcdcp"@if(Auth::user()->healthcard == 'hcdcp') {  selected="selected" } @endif>Health Care & Development Corporationof the PhiliPPines</option>
										<option value="hmi"@if(Auth::user()->healthcard == 'hmi') {  selected="selected" } @endif>Health Maintenance, lnc.</option>
										<option value="imswci"@if(Auth::user()->healthcard == 'imswci') {  selected="selected" } @endif>IMS Wellth Care, lnc.</option>
										<option value="ihci"@if(Auth::user()->healthcard == 'ihci') {  selected="selected" } @endif>lnsular Health Care, lnc.</option>
										<option value="kihi"@if(Auth::user()->healthcard == 'kihi') {  selected="selected" } @endif>Kaiser lnternational Healthgroup, lnc.</option>
										<option value="lhi"@if(Auth::user()->healthcard == 'lhi') {  selected="selected" } @endif>Life & Health HMP, lnc.</option>
										<option value="mhci"@if(Auth::user()->healthcard == 'mhci') {  selected="selected" } @endif>Mazan Health Care, lnc.</option>
										<option value="mhc"@if(Auth::user()->healthcard == 'mhc') {  selected="selected" } @endif>Maxicare Healthcare CorP.</option>
										<option value="mhsi"@if(Auth::user()->healthcard == 'mhsi') {  selected="selected" } @endif>Medocare Health Systems, lnc.</option>
										<option value="mpi"@if(Auth::user()->healthcard == 'mpi') {  selected="selected" } @endif>Medicard PhiliPPines, lnc.</option>
										<option value="mpinc"@if(Auth::user()->healthcard == 'mpinc') {  selected="selected" } @endif>MedicarePlus, lnc.</option>
										<option value="mhsinc"@if(Auth::user()->healthcard == 'mhsinc') {  selected="selected" } @endif>Metrocare Health System, lnc.</option>
										<option value="omhsi"@if(Auth::user()->healthcard == 'omhsi') {  selected="selected" } @endif>Optimum Medical and Healthcare Services, lnc.</option>
										<option value="pchci"@if(Auth::user()->healthcard == 'pchci') {  selected="selected" } @endif>Pacific Cross Health Care, lnc.</option>
										<option value="pci"@if(Auth::user()->healthcard == 'pci') {  selected="selected" } @endif>PhilhealthCare, lnc.</option>
										<option value="shsi"@if(Auth::user()->healthcard == 'shsi') {  selected="selected" } @endif>Stotsenberg Healthcare Sytem lnc.</option>
										<option value="tmdci"@if(Auth::user()->healthcard == 'tmdci') {  selected="selected" } @endif>Transnational Medical & Diagnostic Center,lnc.</option>
										<option value="vchsi"@if(Auth::user()->healthcard == 'vchsi') {  selected="selected" } @endif>Value Care Health SYstems, lnc.</option>
									</select>
								</div> -->

								<div class="form-group">
									<label >Education & Training</label> (optional)
									@if ($errors->has('education'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('education') }}</strong>
									</div>
									@endif
									<textarea class="form-control" name="education" placeholder="Education & training : <?="\n"?> <?="\n"?> - Medical School - Example University <?="\n"?> - Internship - Example Hospital " style="height: 200px;">{{Auth::user()->education}}</textarea>
								</div>
							</div>
							<hr>
								<div class="reset-button">
									<!-- <a href="#" class="btn btn-warning">Reset</a> -->
									<button type="submit" class="btn btn-success pull-right btn-save">Save</button>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section> <!-- /.content -->


	@endsection
