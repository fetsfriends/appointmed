@extends('design.mainlayout')

@section('content')

<!--Start breadcrumb area-->
<section class="breadcrumb-area" style="background-image: url(images/resources/breadcrumb-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="breadcrumbs">
					<h1>Doctor Login</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumb-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="left pull-left">
						<ul>
							<li><a href="index-2.html">Home</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
							<li class="active">Doctor Login</li>
						</ul>
					</div>
					<div class="right pull-right">
						<a href="#">
							<span><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start login register area-->

<section class="login-register-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form">
					<div class="sec-title">
						<h1>Login Now</h1>
						<span class="border"></span>
					</div>
					@if (session('status'))
					    <div class="alert alert-danger">
					        {{ session('status') }}
					    </div>
					@endif
					<div class="row">
						<form class="form-horizontal" method="POST" action="{{url('/doctor-login')}}">
							{{ csrf_field() }}
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('email'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('email') }}</strong>
									</div>
									@endif
									<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Your Email *" required>

									<div class="icon-holder">
										<i class="fa fa-envelope" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('password'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('password') }}</strong>
									</div>
									@endif
									<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  value="{{ old('password') }}" placeholder="Your password" required>
									<div class="icon-holder">
										<i class="fa fa-unlock-alt" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="row">

									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="remember-text">
											<div class="checkbox">
												<label>
													<input name="remember" type="checkbox">
													<span>Remember Me</span>
												</label>
											</div>
										</div>
										<button class="thm-btn bgclr-1" type="submit">Login Now</button>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<a class="forgot-password" href="#">Forgot Password?</a>
										{{-- <ul class="social-icon">
											<li class="login-with">Or login with</li>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter twitter" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus gplus" aria-hidden="true"></i></a></li>
										</ul> --}}
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--End login register area-->

@endsection
