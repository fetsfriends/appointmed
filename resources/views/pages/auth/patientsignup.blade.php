@extends('design.mainlayout')

@section('content')

<section class="login-register-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form register">
					<div class="sec-title">
						<h1>Register Here</h1>
						<span class="border"></span>
					</div>
					<div class="row">
						<form role="form" method="POST" action="{{ url('/register') }}">
							{!! csrf_field() !!}
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('name'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Your Name*" required>
									<div class="icon-holder">
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('contact'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('contact') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" value="{{ old('contact') }}" placeholder="Contact Number*" required>
									<div class="icon-holder">
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('email'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('email') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
									name="email"
									value="{{ old('email') }}"
									placeholder="Your Email*"
									required>

									<div class="icon-holder">
										<i class="fa fa-envelope" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-field">
									@if ($errors->has('gender'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('gender') }}</strong>
									</div>
									@endif
									<select  style="height: 50px;" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" value="{{ old('gender') }}" required>
										<option value="">~~ SELECT GENDER ~~</option>
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
									<div class="icon-holder">
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-field">
									@if ($errors->has('age'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('age') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" value="{{ old('age') }}" placeholder="Age*" required>
									<div class="icon-holder">
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('dob'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('dob') }}</strong>
									</div>
									@endif
									<input type="text" class="form-control{{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob" value="{{ old('dob') }}"  placeholder="Date of Birth" id="datepicker" width="276" autocomplete="off"/>
									<div class="icon-holder">
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('password'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('password') }}</strong>
									</div>
									@endif
									<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
									name="password"
									value="{{ old('password') }}"
									placeholder="Your Password*"
									required>

									<div class="icon-holder">
										<i class="fa fa-unlock-alt" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="input-field">
									@if ($errors->has('password_confirmation'))
									<div class="invalid-feedback">
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</div>
									@endif
									<input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
									name="password_confirmation"
									value="{{ old('password_confirmation') }}"
									placeholder="Confirm Your Password*"
									required>

									<div class="icon-holder">
										<i class="fa fa-unlock-alt" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-lg-5 col-md-3 col-sm-4 col-xs-12">
										<button class="thm-btn bgclr-1" type="submit">Register Here</button>
									</div>
									<div class="col-lg-7 col-md-9 col-sm-8 col-xs-12">
										<h6><span>*</span>You must be a free registered user to submit content. </h6>
									</div>
								</div>
							</div>

						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
</section>

</div>
</div>
</section>


@endsection
