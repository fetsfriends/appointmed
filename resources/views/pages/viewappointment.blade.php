@extends('design.mainlayout')

@section('content')

	<!--Start welcome area-->
<section class="welcome-area">
    <div class="container">
        <div class="row">
            <!-- <div class="col-md-6">
                <div class="img-holder">
                    <img src="{{asset('images/resources/welcome.jpg')}}" alt="Awesome Image">
                </div>
                <div class="inner-content">
                    <p>As a tertiary referral ICU to provide state of the art care with the help of very good professionals and infrastructure.</p>
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="text-holder">
                    <div class="single-testimonial-item text-center">
		                    <div class="img-box" >
		                        <div class="img-holder">
                              <img src="{{asset('doctor_image').'/'.$data->img_name}}" alt="Awesome Image" style="width: 100%; !Important; height: 100%; !important;">
		                            <!-- <img src="" alt="Awesome Image" style="width: 100%; !Important; height: 100%; !important;"> -->
		                        </div>
		                        <div class="quote-box">
		                            <i class="fa fa-calendar" aria-hidden="true"></i>
		                        </div>
		                    </div>
		                    <div class="text-holder">
		                        <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
		                    </div>
		                    <div class="name">
                          <!-- <h3>Full name</h3>
                          <h3>Hospital Name</h3>
                          <h3>Full name</h3> -->
		                        <h3>{{$data->fullname}}</h3>
                            <span>{{$data->hospitalname}},</span>
		                        <span>{{$data->city}}</span>
                            <hr>
                            <h3>Date:</h3> <span> {{$time->schedule_date}}</span>
                            <h3>Time: </h3><span>{{$time->schedule_time}}</span>
                            <hr>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                              <a class="thm-btn bgclr-1 btn-appoint"data-schedid="{{$time->id}}" data-docid="{{$time->doctor_id}}" data-date="{{$time->schedule_date}}" data-time="{{$time->schedule_time}}" href="#">Continue</a>
                            </div>
                            <div class="col-md-4">
                            </div>

                            <!-- <div class="form-group col-md-3 col-md-offset-4">
                              <input type="text" class="btn btn-info test" id="datepicker1" name="sched_date" data-ids = "{{$data->id}}" placeholder="Pick a date" autocomplete="off"style="cursor: pointer">
                            </div> -->
		                    </div>



		                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End welcome area-->



@endsection
