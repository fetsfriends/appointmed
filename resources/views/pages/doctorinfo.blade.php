@extends('design.mainlayout')

@section('content')

	<!--Start welcome area-->
<section class="welcome-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="img-holder">
                    <img src="{{asset('images/resources/welcome.jpg')}}" alt="Awesome Image">
                </div>
                <div class="inner-content">
                    <p>As a tertiary referral ICU to provide state of the art care with the help of very good professionals and infrastructure.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-holder">
                    <div class="single-testimonial-item text-center">
		                    <div class="img-box" >
		                        <div class="img-holder">
		                            <img src="{{asset('doctor_image').'/'.$data->img_name}}" alt="Awesome Image" style="width: 100%; !Important; height: 100%; !important;">
		                        </div>
		                        <div class="quote-box">
		                            <i class="fa fa-calendar" aria-hidden="true"></i>
		                        </div>
		                    </div>
		                    <div class="text-holder">
		                        <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
		                    </div>
		                    <div class="name">
		                        <h3>{{$data->fullname}}</h3>
                            <span>{{$data->hospitalname}},</span>
		                        <span>{{$data->city}}</span>
                            <hr>
                            <div class="form-group col-md-3 col-md-offset-4">
                              <input type="text" class="btn btn-info test" id="datepicker1" name="sched_date" data-ids = "{{$data->id}}" placeholder="Pick a date" autocomplete="off"style="cursor: pointer">
                            </div>
		                    </div>
		                    <div class="schedule-list row">
                          @if($time->isEmpty())
                          <div class="col-md-12">
                            <span>No Schedule for this day</span>
                          </div>
                          @else
                          @foreach($time as $tm)
                          @if($tm->status == 0)
		                    	<div class="button col-md-6">
			                        <a class="thm-btn bgclr-1" href="{{url('/view-appointment').'/?&schedule_date='.$tm->schedule_date.'&schedule_time='.$tm->schedule_time.'&slug='.$data->hash_url}}">{{$tm->schedule_time}}</a>
			                    </div>
                          @elseif($tm->status == 2)
                          <div class="button col-md-6">
                              <a class="thm-btn bgclr-1 disable" href="#"></i> {{$tm->schedule_time}} <i style="color:green;" class="fa fa-check" aria-hidden="true"></i></a>
                          </div>
                          @else
                          <div class="button col-md-6">
                              <a class="thm-btn bgclr-1 disable" href="#">{{$tm->schedule_time}}</a>
                          </div>
                          @endif
			                   @endforeach
                         @endif
		                    </div>


		                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End welcome area-->



@endsection
