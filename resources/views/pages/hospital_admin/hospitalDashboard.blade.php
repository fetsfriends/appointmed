@extends('pages.hospital_admin.hospital_layout.hospital_design')

@section('content')
<!-- Site wrapper -->
<div class="wrapper">
@include('pages.hospital_admin.hospital_layout.hospital_header')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>
			<div class="header-icon">
				<i class="fa fa-tachometer"></i>
			</div>
			<div class="header-title">
				<h1>Hospital Dashboard</h1>
				<small> Dashboard features</small>
				<ol class="breadcrumb hidden-xs">
					<li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
					<li class="active">Dashboard</li>
				</ol>
			</div>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                        	<div class="panel panel-bd cardbox">
                        		<div class="panel-body">
                        			<div class="statistic-box">
                        				<h2><span class="count-number">6</span>
                        				</h2>
                        			</div>
                        			<div class="items pull-left">
                        				<i class="fa fa-user-circle fa-2x"></i>
                        				<h4> Total Appoinments</h4>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                        	<div class="panel panel-bd cardbox">
                        		<div class="panel-body">
                        			<div class="statistic-box">
                        				<h2><span class="count-number">3</span>
                        				</h2>
                        			</div>
                        			<div class="items pull-left">
                        				<i class="fa fa-users fa-2x"></i>
                        				<h4>Patient History</h4>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                        	<div class="panel panel-bd cardbox">
                        		<div class="panel-body">
                        			<div class="statistic-box">
                        				<h2><span class="count-number">4</span>
                        				</h2>
                        			</div>
                        			<div class="items pull-left">
                        				<i class="fa fa-users fa-2x"></i>
                        				<h4>Schedule today</h4>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                        	<div class="panel panel-bd cardbox">
                        		<div class="panel-body">
                        			<div class="statistic-box">
                        				<h2><span class="count-number">7</span>
                        				</h2>
                        			</div>
                        			<div class="items pull-left">
                        				<i class="fa fa-users fa-2x"></i>
                        				<h4>Cancelled</h4>
                        				<h4>Appointment</h4>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-resposive">
                              <div class="panel panel-default">
                                    <a href="/hospital/doctor-list" class="btn pull-right">View All</a>
                                    <div class="panel-heading">
                                          Doctor List
                                    </div>
																		<div class="text-center">No Data Found</div>
                                    <!-- <table class="table table-hover table-bordered">
                                          <thead>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Contact</th>
                                                <th>Action</th>
                                          </thead>
                                          <tbody>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                                <tr>
                                                      <td>Fets Fetalino</td>
                                                      <td>Fets@gmail.com</td>
                                                      <td>09068125453</td>
                                                      <td><a href="#" class="btn btn-danger">Remove</a></td>
                                                </tr>
                                          </tbody>
                                    </table> -->
                              </div>
                        </div>
                    </div>
                </section> <!-- /.content -->

            </div> <!-- /.content-wrapper -->
            <footer class="main-footer">
            	<strong>Copyright &copy; 2016-2017 <a href="#">Appointmed</a>.</strong> All rights reserved.
            </footer>
        </div> <!-- ./wrapper -->
        <!-- ./wrapper -->

@endsection
