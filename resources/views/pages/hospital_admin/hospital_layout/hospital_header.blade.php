<header class="main-header">
  <a href="{{url('/hospital/dashboard')}}" class="logo"> <!-- Logo -->
    <span class="logo-mini">
      <!--<b>A</b>H-admin-->
      <img src="{{ asset('doctors-assets/dist/img/mini-logo.png') }}" alt="">
    </span>
    <span class="logo-lg">
      <!--<b>Admin</b>H-admin-->
      <img src="{{asset('images/resources/logo.jpg')}}" alt="">
    </span>
  </a>
  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top ">
    <form action="/logout" method="post">
      {{csrf_field()}}
      <div class="col-md-6">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-tasks"></span>
        </a>
      </div>
      <div class="col-md-6 ">
          <button type="submit" class="btn btn-danger pull-right" style="margin-top: 15px;">Logout</button>
      </form>
    </div>
  </nav>
</header>
