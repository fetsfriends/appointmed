@extends('pages.hospital_admin.hospital_layout.hospital_design')

@section('content')
@include('pages.hospital_admin.hospital_layout.hospital_header')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<div class="header-icon">
			<i class="fa fa-tachometer"></i>
		</div>
		<div class="header-title">
			<h1> Add Doctors</h1>
			<small> Dashboard features</small>
			<ol class="breadcrumb hidden-xs">
				<li><a href="#"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd lobidrag">
					<div class="panel-body">
						@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif
						<form class="col-sm-6" method="POST" action="/hospital/add-doctor" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								@if ($errors->has('fullname'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('fullname') }}</strong>
								</div>
								@endif
								<label>Full Name</label>
								<input type="text" name="fullname" class="form-control" placeholder="Full Name" required>
							</div>
							<div class="form-group">
								@if ($errors->has('email'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('email') }}</strong>
								</div>
								@endif
								<label >Email</label>
								<input type="email" class="form-control" name="email" placeholder="Email" required>
							</div>
							<div class="form-group">
								@if ($errors->has('roomnumber'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('roomnumber') }}</strong>
								</div>
								@endif
								<label >Room Number</label>
								<input type="text" class="form-control" name="roomnumber" placeholder="Room Number" required>
							</div>
							<div class="form-group">
								<label >Department</label>
								<select class="form-control" name="department" placeholder="Pick Department">
									<option value="cardiology">Cardiology</option>
									<option value="pulmonology">Pulmonology</option>
									<option value="gynecology">Gynecology</option>
									<option value="neurology">Neurology</option>
								</select>
							</div>
							<!-- <div class="form-group">
								@if ($errors->has('contact'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('contact') }}</strong>
								</div>
								@endif
								<label >Contact</label>
								<input type="number" class="form-control" name="contact" placeholder="Contact" required>
							</div> -->
							<!-- <div class="form-group">
								@if ($errors->has('description'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('description') }}</strong>
								</div>
								@endif
								<label >Description</label>
								<input type="textarea" class="form-control" name="description" placeholder="description" required>
							</div> -->
							<div class="form-group">
								@if ($errors->has('password'))
								<div class="invalid-feedback">
									<strong>{{ $errors->first('password') }}</strong>
								</div>
								@endif
								<label>Password</label>
								<input type="password" class="form-control" name="password" placeholder="Password" required>
							</div>
							<div class="form-group">
								<label>Confirm Passowrd</label>
								<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
							</div>
							<!-- <div class="form-group">
								<label>Upload Image</label>
								<input type="file" name="image_name" class="form-control">
							</div> -->
							<div class="reset-button">
								<a href="#" class="btn btn-warning">Reset</a>
								<input type="submit" name="submit" value="Submit" class="btn btn-success">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section> <!-- /.content -->


	@endsection
