@extends('pages.hospital_admin.hospital_layout.hospital_design')

@section('content')
@include('pages.hospital_admin.hospital_layout.hospital_header')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<div class="header-icon">
			<i class="fa fa-tachometer"></i>
		</div>
		<div class="header-title">
			<h1> List of Doctors</h1>
			<small> Dashboard features</small>
			<ol class="breadcrumb hidden-xs">
				<li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd lobidrag">
					<div class="panel-body">
						<div class="row">
							<!-- Profile widget -->
							@if($data->isEmpty())
							<div class="alert alert-danger text-center">No Data Found</div>
							@else
							@foreach($data as $dt)
							<div class="col-sm-12 col-md-4">
								<div class="profile-widget">
									<div class="panel panel-bd">
										<div class="panel-body">
											<div class="media">
												<a class="pull-left" href="#">
													<img class="media-object img-circle" src="{{ asset('doctor_image').'/'.$dt->img_name }}" alt="">
												</a>
												<div class="media-body">
													<h2 class="media-heading">{{$dt->fullname}}</h2>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

		</section> <!-- /.content -->


		@endsection
