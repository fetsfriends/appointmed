<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointmed - Register</title>
    <link rel="shortcut icon" href="{{ asset('doctors-assets/dist/img/ico/favicon.png') }}" type="image/x-icon">
    <link href="{{ asset('doctors-assets/bootstrap/css/bootstrap.min.c') }}ss" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('doctors-assets/pe-icon-7-stroke/css/pe-icon-7-stroke.cs') }}s" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('doctors-assets/dist/css/stylehealth.min.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- Content Wrapper -->
    <div class="login-wrapper">
        <div class="container-center lg">
            <div class="panel panel-bd">
                <div class="panel-heading">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe-7s-unlock"></i>
                        </div>
                        <div class="header-title">
                            <h3>Register</h3>
                            <small><strong>Please enter your data to register.</strong></small>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="{{ url('/hospital/register') }}" method="POST" id="loginForm" novalidate>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-lg-6">
                                @if ($errors->has('hospitalname'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('hospitalname') }}*</strong>
                                </div>
                                @endif
                                <label>Hospital Name</label>
                                <input type="text" value="" id="username" value="{{ old('hospitalname') }}" class="form-control" name="hospitalname" placeholder="Hospital Name">
                            </div>
                            <div class="form-group col-lg-6">
                                @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('email') }}*</strong>
                                </div>
                                @endif
                                <label>Contact Email</label>
                                <input type="email" id="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="Contact Email">
                            </div>
                            <div class="form-group col-lg-6">
                                @if ($errors->has('contactperson'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('contactperson') }}*</strong>
                                </div>
                                @endif
                                <label>Contact Person</label>
                                <input type="text" id="contactperson" class="form-control" value="{{ old('contactperson') }}" name="contactperson" placeholder="Contact Person">
                            </div>
                            <div class="form-group col-lg-12">
                                @if ($errors->has('address'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('address') }}*</strong>
                                </div>
                                @endif
                                <label>Address</label>
                                <input type="text" value="" id="password" class="form-control" value="{{ old('address') }}" name="address" placeholder="Address">
                            </div>
                            <div class="form-group col-lg-6">
                                @if ($errors->has('contact'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('contact') }}</strong>
                                </div>
                                @endif
                                <label>Contact Number*</label>
                                <input type="text" class="form-control" value="{{ old('contact') }}" name="contact" placeholder="Contact Number">
                            </div>

                            <div class="form-group col-lg-6">
                                @if ($errors->has('city'))
                                <div class="invalid-feedback">
                                    <strong style="color: red;">{{ $errors->first('city') }}*</strong>
                                </div>
                                @endif
                                <label>City</label>
                                <input type="text" value="" id="password" class="form-control" value="{{ old('city') }}" name="city" placeholder="City">
                            </div>
                            <div class="form-group col-lg-6">
                                @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                                @endif
                                <label>Password</label>
                                <input type="password" value="" id="repeatpassword" class="form-control" name="password" placeholder="Password">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Confirm Password</label>
                                <input type="password" value="" id="repeatpassword" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-warning">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('doctors-assets/plugins/jQuery/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
</body>

</html>
