@extends('design.mainlayout')

@section('content')

<section class="breadcrumb-area" style="background-image: url(images/resources/breadcrumb-bg.jpg);">
	<div class="container">
       <div class="row">
           <div class="col-md-12">
               <div class="breadcrumbs">
                   <h1>About Us</h1>
               </div>
           </div>
       </div>
   </div>
   <div class="breadcrumb-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="left pull-left">
                    <ul>
                        <li><a href="index-2.html">Home</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                        <li class="active">About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<section class="team-area doctor">
    <div class="container">
        <div class="sec-title mar0auto text-center">
            <h1>About Appointmed</h1>
            <span class="border"></span>
        </div>
				<p class="text-justify">Appointmed is a seamless online platform that allows you to instantly and conveniently find a doctor for your healthcare needs. With only a few clicks, you can get a confirmed appointment with a doctor near you. </p>
        <p class="text-justify">Appointmed was founded by patients like you who know all too well the challenges of making an appointment, the frustrations of long lines, and the concern of finding the right doctor. These experiences spurred them to create Appointmed, a platform they themselves would use and which they envision will also help other patients like you to instantly find a doctor and book an appointment at your convenience. </p>
    </div>
</section>

<!--Start fact counter area-->
<section class="fact-counter-area" style="background-image:url(images/resources/fact-counter-bg.jpg);">
    <div class="container">
        <div class="sec-title text-center col-md-6">
            <h1>Our <span>Mission</span></h1>
            <p>For every online Filipino to have access to timely and targeted health care</p>
        </div>
        <div class="sec-title text-center col-md-6">
            <h1>Our <span>Vision</span></h1>
            <p>To connect patients with the right doctors and healthcare via a seamless online platform</p>
        </div>
    </div>
</section>
<!--End fact counter area-->
<section class="testimonial-page">
    <div class="container">
        <div class="sec-title mar0auto text-center">
            <h1>Meet Our Appointmed Team</h1>
            <span class="border"></span>
        </div>
        <div class="row masonary-layout">
            <!--Start single testimonial item-->
            <div class="col-md-6">
                <div class="single-testimonial-item text-center">
                    <div class="img-box">
                        <div class="img-holder">
                            <img src="images/testimonial/1.png" alt="Awesome Image">
                        </div>
                        <div class="quote-box">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>Mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.</p>
                    </div>
                    <div class="name">
                        <h3>Rossy Miranda</h3>
                        <span>CEO</span>
                    </div>
                </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="col-md-6">
                <div class="single-testimonial-item text-center">
                    <div class="img-box">
                        <div class="img-holder">
                            <img src="images/testimonial/2.png" alt="Awesome Image">
                        </div>
                        <div class="quote-box">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>The master-builder of human happiness one rejects, dislikes, or avoids pleasure itself, because it is pleasure pursue.</p>
                    </div>
                    <div class="name">
                        <h3>Peter Lawrence</h3>
                        <span>Founder</span>
                    </div>
                </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="col-md-6">
                <div class="single-testimonial-item text-center">
                    <div class="img-box">
                        <div class="img-holder">
                            <img src="images/testimonial/3.png" alt="Awesome Image">
                        </div>
                        <div class="quote-box">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of and expound the actual teachings.</p>
                    </div>
                    <div class="name">
                        <h3>Aldwen Shannen</h3>
                        <span>Chief Finncial Officer</span>
                    </div>
                </div>
            </div>
            <!--End single testimonial item-->
            <!--Start single testimonial item-->
            <div class="col-md-6">
                <div class="single-testimonial-item text-center">
                    <div class="img-box">
                        <div class="img-holder">
                            <img src="images/testimonial/3.png" alt="Awesome Image">
                        </div>
                        <div class="quote-box">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of and expound the actual teachings.</p>
                    </div>
                    <div class="name">
                        <h3>Aldwen Shannen</h3>
                        <span>Chief Technology Officer</span>
                    </div>
                </div>
            </div>
            <!--End single testimonial item-->
            <!--End single testimonial item-->
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <ul class="post-pagination text-center">
                    <li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div> -->
    </div>
</section>



@endsection
