<!-- <div class="row">
      <div class="col-md-12">
          <ul class="post-pagination text-center">
              <li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
          </ul>
      </div>
  </div> -->

  @if ($paginator->lastPage() > 1)
  <ul class="post-pagination text-center">
      <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
          <a href="{{ $paginator->url(1) }}"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
      </li>
      @for ($i = 1; $i <= $paginator->lastPage(); $i++)
          <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
              <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
          </li>
      @endfor
      <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
          <a href="{{ $paginator->url($paginator->currentPage()+1) }}" ><i class="fa fa-caret-right" aria-hidden="true"></i></a>
      </li>
  </ul>
  @endif
