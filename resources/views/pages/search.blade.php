@extends('design.mainlayout')

@section('content')
<?php ?>
<!--Start blog Single area-->
<section id="blog-area" class="blog-single-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
        <div class="sec-title pdb-30">
          <h3>Your doctor searched!</h3>
          <span class="border"></span>
        </div>
        <div class="blog-post">
          <!--Start author box-->
          @foreach($data as $dt)
          <div class="author-box" data-url="{{url('/doctorinfo').'/'.$dt->hash_url}}" style="cursor: pointer;">
            <div class="row">
              <div class="col-md-12">

                <div class="img-holder">
                  <img class="radius" src="{{asset('doctor_image').'/'.$dt->img_name}}" alt="Awesome Image">
                </div>
                <div class="text-holder">
                  {{-- <div class="review-box pull-right">
                    <ul>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                    </ul>
                  </div> --}}
                  <h3>{{$dt->fullname}}</h3>
                  <strong><p>{{$dt->specialization.' | '.$dt->hospitalname}}</p></strong>
                  <p>We will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                  <strong><p>{{$dt->address}}</p></strong>
                  <!-- <ul class="social-link">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                  </ul> -->
                </div>
              </div>
            </div>
          </div>
          <hr>
          @endforeach
          {{$data->links('pages.pagination')}}
          <!--End author box-->
        </div>
      </div>
    </div>
  </div>
</section>
<!--End blog Single area-->




@endsection
