`<!--Start Preloader -->
<div class="preloader"></div>
<!--End Preloader -->

<!--Start header area-->

<!--Start top bar area-->
<section class="top-bar-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                <div class="top-left">
                    <p><span class="flaticon-phone"></span>24 hours set an Appointment Service: 09195643707</p>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="top-right clearfix">
                    <ul class="social-links">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End top bar area-->

<!--Start header area-->
<section class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <img src="{{asset('images/resources/logo.jpg')}}" alt="Awesome Logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="header-right">
                    {{-- <ul>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-technology"></span>
                            </div>
                            <div class="text-holder">
                                <h4>Call us now</h4>
                                <span>+1-888-987-6543</span>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-pin"></span>
                            </div>
                            <div class="text-holder">
                                <h4>Metro Manila</h4>
                                <span>1550 Mandaluyong City</span>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-agenda"></span>
                            </div>
                            <div class="text-holder">
                                <h4>Mon - Satday</h4>
                                <span>09.00am to 18.00pm</span>
                            </div>
                        </li>
                    </ul> --}}
                    <div class="search-button pull-right">
                        <div class="toggle-search">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End header area-->

<!--Start header-search  area-->
<section class="header-search">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="search-form pull-right">
                    <form action="#">
                        <div class="search">
                            <input type="search" name="search" value="" placeholder="Search">
                            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End header-search  area-->

<!--Start mainmenu area-->
<section class="mainmenu-area stricky">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!--Start mainmenu-->
                <nav class="main-menu pull-left">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="current"><a href="{{ url('/') }}">Home</a></li>
                            <li class=""><a href="{{ url('/about') }}">About Us</a></li>
                            <li class=""><a href="{{ url('/contact') }}">Contact Us</a></li>
                            @if(Auth::check())
                            <li class=""><a href="{{ url('/appointment-list') }}">Appointment List</a></li>
                            @endif
                            <!-- <li class="dropdown"><a href="about.html">About Us</a>
                                <ul>
                                    <li><a href="about.html">About Hospitals</a></li>
                                    <li><a href="doctors.html">Meet Our Doctors</a></li>
                                    <li><a href="faq.html">FAQ’s</a></li>
                                    <li><a href="testimonials.html">Testimonials</a></li>
                                    <li><a href="project.html">Our Gallery</a></li>
                                    <li><a href="project-single.html">Gallery Single</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="departments.html">Departments</a>
                                <ul>
                                    <li><a href="cardiac-clinic.html">Cardiac Clinic</a></li>
                                    <li><a href="pulmonology.html">Pulmonology</a></li>
                                    <li><a href="gynecology.html">Gynecology</a></li>
                                    <li><a href="neurology.html">Neurology</a></li>
                                    <li><a href="urology.html">Urology</a></li>
                                    <li><a href="gastrology.html">Gastrology</a></li>
                                    <li><a href="pediatrician.html">Pediatrician</a></li>
                                    <li><a href="laborotory.html">Laborotory</a></li>
                                </ul>
                            </li> -->
                            {{-- <li><a href="time-table.html">Time Table</a></li>
                            <li class="dropdown"><a href="blog-default.html">News</a>
                                <ul>
                                    <li><a href="blog-default.html">News Default</a></li>
                                    <li><a href="blog-large.html">News Large View</a></li>
                                    <li><a href="blog-single.html">News Single Post</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="shop.html">Shop</a>
                                <ul>
                                    <li><a href="shop.html">Shop Products</a></li>
                                    <li><a href="shop-single.html">Products Single</a></li>
                                    <li><a href="shopping-cart.html">Shopping Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="account.html">My Account</a></li>
                                </ul>
                            </li> --}}
                            <!-- <li><a href="contact.html">Contact Us</a></li> -->
                        </ul>
                    </div>
                </nav>
                <!--End mainmenu-->
                <!--Start mainmenu right box-->


                        @if(Auth::check())
                        <div class="mainmenu-right-box pull-right">
                        <div class="consultation-button">
                          <form action="{{url('/_logout')}}">
                            {{csrf_field()}}
                            <a href="{{ url('/_logout') }}">Log Out</a>
                          </form>
                        </div>
                      </div>
                        @else
                        <div class="mainmenu-right-box pull-right">
                          <div class="dropdown">
                            <button class="dropbtn">Log in/ Sign up</button>
                            <div class="dropdown-content">
                              <a href="{{ url('/account') }}">Patient: Login/Signup</a>
                              <a href="{{ url('/doctor-login') }}">Doctor: Login</a>
                            </div>
                          </div>
                        </div>
                        @endif


                <!--End mainmenu right box-->
            </div>
        </div>
    </div>
</section>
<!--End mainmenu area-->
