<!--Start footer area-->
<footer class="footer-area">
  <div class="container">
    <div class="row">
      <!--Start single footer widget-->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="single-footer-widget pd-bottom50">
          <div class="title">
            <h3>About Appointmed</h3>
            <span class="border"></span>
          </div>
          <div class="our-info">
            <p>Appointmed is a seamless online platform that allows you to instantly and conveniently find a doctor for your healthcare needs. With only a few clicks, you can get a confirmed appointment with a doctor near you. </p>
            <p class="mar-top">Appointmed was founded by patients like you who know all too well the challenges of making an appointment, the frustrations of long lines, and the concern of finding the right doctor. These experiences spurred them to create Appointmed, a platform they themselves would use and which they envision will also help other patients like you to instantly find a doctor and book an appointment at your convenience. </p>
            <!-- <a href="#">Know More<i class="fa fa-caret-right" aria-hidden="true"></i></a> -->
          </div>
        </div>
      </div>
      <!--End single footer widget-->
      <!--Start single footer widget-->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <!-- <div class="single-footer-widget pd-bottom50">
        <div class="title">
        <h3>Useful Links</h3>
        <span class="border"></span>
      </div>
      <ul class="usefull-links fl-lft">
      <li><a href="#">About Us</a></li>
      <li><a href="#">Awards</a></li>
      <li><a href="#">Consultants</a></li>
      <li><a href="#">Working Hours</a></li>
      <li><a href="#">Procedures</a></li>
      <li><a href="#">Special Offers</a></li>
      <li><a href="#">FAQ’s</a></li>
    </ul>
    <ul class="usefull-links">
    <li><a href="#">Services</a></li>
    <li><a href="#">Healthy Foods</a></li>
    <li><a href="#">Appointments</a></li>
    <li><a href="#">Latest News</a></li>
    <li><a href="#">Certificates</a></li>
    <li><a href="#">Qualifications</a></li>
  </ul>
</div> -->
</div>
<!--End single footer widget-->
<!--Start single footer widget-->
<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
  <div class="single-footer-widget mar-bottom">
    <div class="title">
      <h3>Contact Details</h3>
      <span class="border"></span>
    </div>
    <ul class="footer-contact-info">
      <li>
        <div class="icon-holder">
          <span class="flaticon-pin"></span>
        </div>
        <div class="text-holder">
          <h5> 71 P. Tuazon Blvd
            Cubao, Quezon City, 1111<br> Metro Manila</h5>
          </div>
        </li>
        <li>
          <div class="icon-holder">
            <span class="flaticon-interface"></span>
          </div>
          <div class="text-holder">
            <h5>email@appointmed.com</h5>
          </div>
        </li>
        <li>
          <div class="icon-holder">
            <span class="flaticon-technology-1"></span>
          </div>
          <div class="text-holder">
            <h5>09999999999</h5>
          </div>
        </li>
        <li>
          <div class="icon-holder">
            <span class="flaticon-clock"></span>
          </div>
          <div class="text-holder">
            <h5>Mon-Saturday: 9am to 5pm</h5>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <!--Start single footer widget-->
</div>
</div>
</footer>
<!--End footer area-->

<!--Start footer bottom area-->
<section class="footer-bottom-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="copyright-text">
          <p>Copyrights © 2018 All Rights Reserved, Powered by <a href="#">Fets & Kurt Developers</a></p>
        </div>
      </div>
      {{-- <div class="col-md-4">
        <ul class="footer-social-links">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div> --}}
    </div>
  </div>
</section>
<!--End footer bottom area-->
