<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Appointmed</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-16x16.png') }}" sizes="16x16">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/gijgo.css') }}">
	<!-- <link rel="stylesheet" type="text/css" href="{{ asset('fonts/gijgo-material.ttf') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/gijgo-material.woff') }}"> -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/tempusdominus-bootstrap-3.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-confirm.css') }}">
	<!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">



	<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">


	<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}"> -->


</head>
<body>

	@include('design.header')

	@yield('content')

	@include('design.footer')

	<!--Scroll to top-->
	<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-triangle-inside-circle"></span></div>

	<!-- main jQuery -->

	<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>


	<script src="{{ asset('js/moment.js') }}"></script>
	<script src="{{ asset('js/tempusdominus-bootstrap-3.js') }}"></script>
	<script src="{{ asset('js/jquery-confirm.min.js') }}"></script>



	<script src="{{ asset('js/wow.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>

	<script src="{{ asset('js/dev.js') }}"></script>
	<script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
	<script src="{{ asset('js/jquery.countTo.js') }}"></script>
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/validation.js') }}"></script>
	<script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
	<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
	<script src="{{ asset('js/gmaps.js') }}"></script>
	<script src="{{ asset('js/map-helper.js') }}"></script>
	<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
	<script src="{{ asset('js/jquery.appear.js') }}"></script>
	<script src="{{ asset('js/isotope.js') }}"></script>
	<script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
	<script src="{{ asset('js/jquery.bootstrap-touchspin.js') }}"></script>
	{{-- <script src="{{ asset('assets/timepicker/timePicker.js') }}"></script> --}}
	<script src="{{ asset('assets/bootstrap-sl-1.12.1/bootstrap-select.js') }}"></script>
	<script src="{{ asset('assets/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
	<script src="{{ asset('assets/language-switcher/jquery.polyglot.language.switcher.js') }}"></script>
	<script src="{{ asset('assets/html5lightbox/html5lightbox.js') }}"></script>


	{{-- <script src="{{ asset('js/gijgo.js') }}"></script> --}}





	{{-- <script src="{{ asset('js/gijgo.js') }}"></script> --}}

	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>


	{{-- <script src="{{ asset('js/gijgo.js') }}"></script> --}}

	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>




	<!-- revolution slider js -->
	<script src="{{ asset('assets/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
	<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
	<!-- thm custom script -->
	<script src="{{ asset('js/custom.js') }}"></script>
	<script type="text/javascript">
	var APP_URL = {!! json_encode(url('/')) !!}
	</script>
	<!--   -->

</body>
</html>
