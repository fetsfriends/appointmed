<div class="wrapper">
  <aside class="main-sidebar" style="padding-top: 0px !important">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
       <div class="user-panel">
                <div class="image pull-left">
                    <img src="{{ asset('doctor_image').'/'.Auth::user()->img_name }}" class="img-circle" alt="User Image">
                </div>
                <div class="info">
                    <h4>Welcome</h4>
                    <p>{{ Auth::user()->fullname }}</p>
                </div>

          </div>

        <ul class="sidebar-menu">
            <li class="active">
                <a href="/doctor-dashboard"><i class="fa fa-hospital-o"></i><span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i><span>Doctor</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/doctor/add-schedule') }}">Add Schedule</a></li>
                    <li><a href="{{ url('/doctor/edit-doctor') }}">Edit Profile</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i><span>Patient</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- <li><a href="add-patient.html">Add patient</a></li> -->
                    <li><a href="{{ url('/doctor/patient-list') }}">Patient list</a></li>
                    <li><a href="{{ url('/doctor/walkin-list') }}">Walk-in list</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i> <span>Schedule</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/doctor/schedule-list') }}">List schedule</a></li>
                </ul>
            </li>
        </ul>
    </li>

</ul>
</div> <!-- /.sidebar -->
</aside>
