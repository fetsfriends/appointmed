<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('email')->unique();
            $table->string('room_number');
            $table->string('department');
            $table->string('password');
            $table->string('contact')->nullable();
            $table->string('specialization')->nullable();
            $table->string('healthcard')->nullable();
            $table->string('hospital_id');
            $table->string('hash_url')->nullable();
            $table->string('description')->nullable();
            $table->string('img_name')->nullable();
            $table->string('education')->nullable();
            $table->string('sub_specialization')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
