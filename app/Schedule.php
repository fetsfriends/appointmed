<?php

namespace AppointMed;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['schedule_date','schedule_time','doctor_id','status'];
}
