<?php

namespace AppointMed;

use Illuminate\Database\Eloquent\Model;

class Walkin extends Model
{
    protected $fillable = ['fullname','contact','doctor_id','walkin_date','walkin_time','status'];
}
