<?php

namespace AppointMed;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
  protected $fillable = ['appointment_date','appointment_time','doctor_id','user_id','type','status']; 
}
