<?php

namespace AppointMed\Http\Controllers;
use Illuminate\SUpport\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use AppointMed\Helpers\Helper;
use AppointMed\Doctor;
use AppointMed\Schedule;
use AppointMed\Appointment;
class PagesController extends Controller
{
  public function homepage(){
    if (Auth::check()) {
      return redirect(route('patient.dashboard'));
    }
    return view('pages.homepage');
  }

  public function account(){
    return view ('pages.auth.account');
  }

  public function patient(){
    return view('pages.patient');
  }

  public function contactUs(){
    return view('pages.contactus');
  }

  public function aboutUs(){
    return view('pages.aboutus');
  }

  public function appointmentList(){
    $data = Appointment::where('user_id','=',Auth::user()->id)->join('doctors','doctors.id','=','appointments.doctor_id')->orderBy('appointment_date','DESC')->get();
    return view('pages.appointment-list',compact('data'));
  }

  public function signup(){
    if (Auth::check()) {
      return redirect(route('patient.dashboard'));
    }
    return view('pages.auth.patientsignup');
  }

  public function search(){
    $data = Doctor::where('hash_url','!=',null)->join('hospitals','hospitals.id','=','doctors.hospital_id')->paginate(5);
    // Helper::encrypt_decrypt(1,'encrypt');
    return view('pages.search',compact('data'));
  }

  public function doctorinfo($code){
    $id = Crypt::decrypt($code);
    $date = date("F, d o");
    $data = Doctor::select('doctors.*','hospitals.hospitalname','hospitals.city','hospitals.address')->where('doctors.id','=',$id)->join('hospitals','hospitals.id','=','doctors.hospital_id')->first();
    $time = Schedule::where('doctor_id','=',$id)->where('schedule_date','=',$date)->get();
    return view('pages.doctorinfo',compact('data','time'));
  }

  public function mylogout(){
    Auth::logout();
    return redirect('/');
  }

  public function appointment(){
    $this->validate(request(),[
      'appointment_date' => 'required',
      'appointment_time' => 'required',
      'doctor_id' => 'required',
      'user_id' => 'required',
    ]);
    $checker = Appointment::where('appointment_date','=',request('date'))->where('appointment_time','=',request('time'))->where('doctor_id','=',request('doc_id'))->first();
      if ($checker->status == 1) {
        $response = array('msg' => 'Appointment Failed!');
      }else{
        $response = array('msg' => 'Appointment Success!');
        Appointment::create([
          'appointment_date' => request('date'),
          'appointment_time' => request('time'),
          'doctor_id' => request('doc_id'),
          'user_id' => Auth::user()->id,
          'type' => 'online',
          // 'user_id' => 1,
          'status' => 1,
        ]);
        $data = Schedule::where('id','=',request('sched_id'))->first();
        $data->status = 1;
        $data->save();
      }
    return Helper::json_format($response);
  }
  public function authchecker(){
    if (Auth::check()) {
      $response = array('msg' => true);
    }else{
      $response = array('msg' => false);
    }
    return Helper::json_format($response);
  }
  public function getdate(){
    $date = date('F, d o',strtotime(request('date')));
    $data = Schedule::where('doctor_id','=',request('doc_id'))->where('schedule_date','=',$date)->join('doctors','doctors.id','=','schedules.doctor_id')->get();
    if ($data->isEmpty()) {
      $data = 'No Schedule for this day';
    }
    return Helper::json_format($data);
  }
  public function viewappointment(){
    // dd(request()->all());
    $id = Crypt::decrypt(request('slug'));
    $date = date("F, d o");
    $data = Doctor::select('doctors.*','hospitals.hospitalname','hospitals.city','hospitals.address')->where('doctors.id','=',$id)->join('hospitals','hospitals.id','=','doctors.hospital_id')->first();
    $time = Schedule::where('doctor_id','=',$id)->where('schedule_date','=',request('schedule_date'))->where('schedule_time','=',request('schedule_time'))->first();
    return view('pages.viewappointment',compact('data','time'));
  }
}
