<?php

namespace AppointMed\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function adminDashboard(){
    	return view('pages.admin.dashboard');
    }
    public function adminLogin(){
    	return view('pages.admin.admin-login');
    }
}
