<?php

namespace AppointMed\Http\Controllers;

use Illuminate\SUpport\Facades\Auth;
use Illuminate\Http\Request;
use AppointMed\Hospital;
use AppointMed\Doctor;


class HospitalController extends Controller
{
    public function hospitalDashboard(){
    	return view('pages.hospital_admin.hospitalDashboard');
    }

    public function showAddDoctor(){
        return view('pages.hospital_admin.hospitalAddDoctor');
    }

    public function hospitalAddDoctor(){

        $this->validate(request(), [
            'fullname' => 'required',
            'email' => 'required|email|unique:doctors,email',
            // 'contact' => 'required',
            'password' => 'required|confirmed',
            // 'image_name' => 'image|mimes:jpg,jpeg,png,gif'
        ]);

        // if (request()->hasFile('image_name')) {
        //     $image = request()->file('image_name');
        //     $fileName = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
        //     $image->move('dd(/doctor_image/', $fileName);
        // }

            Doctor::create([
                'fullname' => request('fullname'),
                'email' => request('email'),
                'room_number' => request('roomnumber'),
                'department' => request('department'),
                // 'contact' => request('contact'),
                'password' => bcrypt(request('password')),
                'hospital_id' => Auth::user()->id,
                'img_name' => 'default.png',
            ]);

            return back()->with('status', 'Successfully Doctor Added!');
    }

    public function hospitalDoctorList(){
      $data = Hospital::where('hospitals.id','=',Auth::user()->id)->where('hospitals.status','=',1)->join('doctors','doctors.hospital_id','=','hospitals.id')->get();
    	return view('pages.hospital_admin.hospitalDoctorList',compact('data'));
    }

    public function hospitalLogin(){
    	return view('pages.hospital_admin.hospitalLogin');
    }

    public function showRegister(){
       return view('pages.hospital_admin.hospitalRegister');
   }
   public function hospitalRegister(){

        // dd(request()->all());

    $this->validate(request(),[
        'hospitalname' => 'required',
        'email' => 'required',
        'contact' => 'required',
        'password' => 'required|confirmed',
        'address' => 'required',
        'city' => 'required',
    ]);

    Hospital::create([
        'hospitalname' => request('hospitalname'),
        'email' => request('email'),
        'contact' => request('contact'),
        'password' => bcrypt(request('password')),
        'address' => request('address'),
        'city' => request('city'),
        'contactperson' => request('contactperson'),
        'status' => 0,
    ]);

    return redirect('hospital-login')->with('status','Registered Successfully!');
        // if ($request->isMethod('post')) {
        //     $data = $request->input();
        //     $hospital = new hospital;
        //     $hospital->hospitalname = $data['hospitalname'];
        //     $hospital->email = $data['email'];
        //     $hospital->contact = $data['contact'];
        //     $hospital->password = $data['password'];
        //     $hospital->save();

        //     echo "success";
        // }

}
}
