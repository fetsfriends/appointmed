<?php

namespace AppointMed\Http\Controllers\Auth;

use Illuminate\Http\Request;
use AppointMed\Http\Controllers\Controller;
use Auth;
class HospitalLoginController extends Controller
{
  public function __construct(){
    $this->middleware('guest:hospital');
  }
  public function showlogin(){
    return view('pages.hospital_admin.hospitalLogin');
  }
  public function login(){
    $this->validate(request(),[
      'email' => 'required|email',
      'password' => 'required',
    ]);
    if(Auth::guard('hospital')->attempt(['email' => request('email'), 'password' => request('password')],false)){
      return redirect()->intended('/hospital/dashboard');
    }else{
      return back()->with('status', 'Login Failed!');;
    }
  }
}
