<?php

namespace AppointMed\Http\Controllers\Auth;

use Illuminate\Http\Request;
use AppointMed\Http\Controllers\Controller;
use Auth;
class DoctorLoginController extends Controller
{
  public function __construct(){
    $this->middleware('guest:doctor');
  }
  public function showlogin(){
    return view('pages.doctor.doctorLogin');
  }
  public function login(){
    $this->validate(request(),[
      'email' => 'required|email',
      'password' => 'required',
    ]);
    if(Auth::guard('doctor')->attempt(['email' => request('email'), 'password' => request('password')],false)){
      return redirect()->intended('/doctor-dashboard');
    }else{
      return back()->with('status', 'Login Failed!');
    }
  }
}
