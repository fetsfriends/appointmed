<?php

namespace AppointMed\Http\Controllers\Auth;

use AppointMed\User;
use AppointMed\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/patient';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'contact' => 'required|regex:/(09)[0-9]{9}/',
            'gender' => 'required|string|max:255',
            'age' => 'required|integer|max:255',
            'dob' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required ',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \AppointMed\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'contact' => $data['contact'],
            'gender' => $data['gender'],
            'age' => $data['age'],
            'date_of_birth' => $data['dob'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
