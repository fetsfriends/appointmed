<?php

namespace AppointMed\Http\Controllers\Auth;

use Illuminate\Http\Request;
use AppointMed\Http\Controllers\Controller;
use App\User;

use Socialite;

class AuthController extends Controller
{
  public function redirectToProvider()
  {
      return Socialite::driver('facebook')->redirect();
  }

  /**
   * Obtain the user information from GitHub.
   *
   * @return \Illuminate\Http\Response
   */
  public function handleProviderCallback()
  {
      $user = Socialite::driver('facebook')->user();
      $user_data = $user->user;
      // User::create(['
      //   ''
      // ']);
      return $user->user;
  }
}
