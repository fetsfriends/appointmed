<?php

namespace AppointMed\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use AppointMed\Doctor;
use AppointMed\User;
use AppointMed\Appointment;
use AppointMed\Schedule;
use AppointMed\Walkin;
use AppointMed\Helpers\Helper;
class DoctorController extends Controller
{

  public function dashboard(){
    // session_destroy();
    $data_sched = Schedule::where('doctor_id','=',Auth::user()->id)->orderBy('created_at','DESC')->take(10)->get();
    $data_appointment = Appointment::select('users.*','appointments.*','appointments.id as aptid')->where('doctor_id','=',Auth::user()->id)->join('users','appointments.user_id','=','users.id')->orderBy('appointment_date','DESC')->take(6)->get();
    return view('pages.doctor.dashboard',compact('data_sched','data_appointment'));
  }


  public function addSchedule(){
    return view('pages.doctor.addSchedule');
  }

  public function editDoctor(){
    return view('pages.doctor.doctorEdit');
  }
  public function updateDoctor(Doctor $id){
    $healthcard = implode(",",request('healthcard'));
    $this->validate(request(), [
        'email' => 'unique:doctors,email,'.$id->id,
        'contact' => 'required|regex:/(09)[0-9]{9}/',
        'description' => 'required',
        'specialization' => 'required',
        'image_name' => 'mimes:jpg,jpeg,png,gif|nullable'
    ]);
    // dd(request()->all());
    $data = Doctor::where('id','=',$id->id)->first();
    $data->fullname = request('fullname');
    $data->email = request('email');
    $data->contact = request('contact');
    $data->specialization = request('specialization');
    $data->healthcard = $healthcard;
    $data->hash_url = Crypt::encrypt($id->id);
    $data->description = request('description');
    $data->education = request('education');
    $data->sub_specialization = request('sub_specialization');
    if (request()->hasFile('image_name')) {
        $image = request()->file('image_name');
        $fileName = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
        $image->move('./doctor_image/', $fileName);
        $data->img_name = $fileName;
    }
    $data->save();
    return back()->with('status', 'Information Updated Successfully!');
  }
  public function doctorScheduleList(){
    $data = Schedule::where('doctor_id','=',Auth::user()->id)->orderBy('status','ASC')->orderBy('schedule_date','DESC')->paginate(15);
    return view('pages.doctor.doctorScheduleList',compact('data'));
  }
  public function doctorWalkinList(){
    $data = Walkin::where('doctor_id','=',Auth::user()->id)->orderBy('walkin_date','DESC')->get();
    return view('pages.doctor.walkinlist',compact('data'));

  }
  public function doctorPatientList(){
    $data = Appointment::select('users.*','appointments.*','appointments.id as aptid')->where('appointments.doctor_id','=',Auth::user()->id)->join('users','appointments.user_id','=','users.id')->orderBy('status','ASC')->orderBy('appointment_date','DESC')->get();
    return view('pages.doctor.doctorPatientList',compact('data'));
  }
  public function doctorAddPatient(Schedule $id){
    $data = Schedule::where('id','=',$id->id)->first();
    return view('pages.doctor.addPatient',compact('data'));
  }
  public function doctorAddPatientstore(){
    // dd(request()->all());
    $this->validate(request(),[
      'fullname' => 'required',
      'contact' => 'required|regex:/(09)[0-9]{9}/',
    ]);
    Walkin::create([
      'fullname' => request('fullname'),
      'contact' => request('contact'),
      'doctor_id' => Auth::user()->id,
      'walkin_date' => request('date'),
      'walkin_time' => request('time'),
      'status' => 1,
    ]);
    // Appointment::create([
    //   'appointment_date' => request('date'),
    //   'appointment_time' => request('time'),
    //   'doctor_id' => Auth::user()->id,
    //   'type' => 'walk-in',
    //   'status' => 1,
    // ]);
    $upd = Schedule::where('id','=',request('id'))->first();
    $upd->status = 1;
    $upd->save();
    return back()->with('status','Appointment Successful!');
  }
  public function resetPassword(){
    return view('pages.doctor.doctorResetPassword');
  }
  public function setscheduletime($date1,$date2,$per){
    $range=range(strtotime($date1),strtotime($date2),$per*60);
    $data['date'] = $range;
    foreach($data['date'] as $time){
      $test[] =  date("h:i A",$time)."\n";
    }
    return $test;
  }
  public function giveschedule(){
    if(empty(request('time_from')) OR empty(request('time_to')) OR empty(request('interval_time'))){
        return back()->with('failed','Schedule Failed!');
    }
    $data = $this->setscheduletime(request('time_from'),request('time_to'),request('interval_time'));
    if(!empty(request('sched_date'))){
      $date = date('F, d o',strtotime(request('sched_date')));
    }else{
      $date = date('F, d o');
    }
    return view('pages.doctor.checkschedule',compact('data','date'));
  }
  public function store(){
    // $this->validate(request(),[
    //   'schedule_date' => 'required',
    //   'time' => 'required',
    //   'doctor_id' => 'required',
    // ]);
    $load = Schedule::where('schedule_date','=',request('sched_date'))->where('doctor_id','=',Auth::user()->id)->first();
    if(!empty($load)){
      return redirect('/doctor/add-schedule')->with('status', 'Schedule for that day existing!');
    }else{
      $data = request('schedule');
      foreach ($data as $key => $value) {
        Schedule::create([
          'schedule_date' => request('sched_date'),
          'schedule_time' => $value,
          'doctor_id' => Auth::user()->id,
          'status' => 0,
        ]);
      }
      return redirect('/doctor/schedule-list');
    }
  }
  public function update_status_done(Appointment $id){
    $data = Appointment::where('id','=',$id->id)->first();
    $data->status = 2;
    $data->save();
    $response = array('msg' => 'Updated Successfully!');
    return Helper::json_format($response);
  }
  public function update_status_resched(Appointment $id){
    $data = Appointment::where('id','=',$id->id)->first();
    $data->status = 0;
    $data->save();
    $response = array('msg' => 'Updated Successfully!');
    return Helper::json_format($response);
  }
  public function update_status_done_walkin(Walkin $id){
    $data = Walkin::where('id','=',$id->id)->first();
    $data->status = 2;
    $data->save();
    $response = array('msg' => 'Updated Successfully!');
    return Helper::json_format($response);
  }
  public function update_status_resched_walkin(Walkin $id){
    $data = Walkin::where('id','=',$id->id)->first();
    $data->status = 0;
    $data->save();
    $response = array('msg' => 'Updated Successfully!');
    return Helper::json_format($response);
  }
}
