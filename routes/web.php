<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('/homepage');
// });

Auth::routes();
//
Route::match(['get','post'], '/', 'PagesController@homepage');
// Route::get('/testingfb', function () {
//     return view('pages.fbtest');
// });
Route::get('/privacy-policy', function () {
    return view('pages.privacy');
});
Route::match(['get', 'post'],'/account','PagesController@account')->name('account');
Route::get('/_logout','PagesController@mylogout');
Route::get('/test','PagesController@giveschedule');
Route::match(['get', 'post'], '/signup', 'PagesController@signup');
Route::get('/search', 'PagesController@search');
Route::get('/contact', 'PagesController@contactUs');
Route::get('/about', 'PagesController@aboutUs');
Route::get('/doctorinfo/{code}', 'PagesController@doctorinfo');
Route::post('/appointment', 'PagesController@appointment');
Route::post('/getdate', 'PagesController@getdate');


Route::middleware('auth')->group(function() {
  Route::get('/patient', 'PagesController@patient')->name('patient.dashboard');
  Route::get('/appointment-list', 'PagesController@appointmentList');
  Route::get('/view-appointment', 'PagesController@viewappointment');
  // Route::get('/auth/check', 'PagesController@authchecker');
});

Route::middleware('auth:doctor')->group(function() {
  Route::match(['get', 'post'], '/doctor-dashboard', 'DoctorController@dashboard')->name('doctor.dashboard');
  Route::match(['get', 'post'], '/doctor/add-schedule', 'DoctorController@addSchedule');
  Route::get('/doctor/edit-doctor', 'DoctorController@editDoctor');
  Route::put('/doctor/edit-doctor/{id}', 'DoctorController@updateDoctor');
  Route::match(['get', 'post'], '/doctor/reset-password', 'DoctorController@resetPassword');
  Route::match(['get', 'post'], '/doctor/check-time', 'DoctorController@giveschedule');
  Route::match(['get', 'post'], '/doctor/store', 'DoctorController@store');
  Route::get('/doctor/schedule-list', 'DoctorController@doctorScheduleList');
  Route::get('/doctor/patient-list', 'DoctorController@doctorPatientList');
  Route::get('/doctor/walkin-list', 'DoctorController@doctorWalkinList');
  Route::get('/doctor/add-patient/{id}', 'DoctorController@doctorAddPatient');
  Route::put('/doctor/update/{id}', 'DoctorController@update_status_done');
  Route::put('/doctor/update_re/{id}', 'DoctorController@update_status_resched');
  Route::put('/doctor/update_walkin/{id}', 'DoctorController@update_status_done_walkin');
  Route::put('/doctor/update_re_walkin/{id}', 'DoctorController@update_status_resched_walkin');
  Route::post('/doctor/add-patient', 'DoctorController@doctorAddPatientstore');

});

// Doctor Route
Route::get('/doctor-login', 'Auth\DoctorLoginController@showlogin')->name('doctor.login');
Route::post('/doctor-login', 'Auth\DoctorLoginController@login');

Route::middleware('auth:hospital')->group(function(){
	Route::match(['get', 'post'], '/hospital/dashboard', 'HospitalController@hospitalDashboard')->name('hospital.dashboard');
	Route::match(['get', 'post'], '/hospital/doctor-list', 'HospitalController@hospitalDoctorList');
  Route::post('/hospital/add-doctor', 'HospitalController@hospitalAddDoctor');
	Route::get('/hospital/add-doctor', 'HospitalController@showAddDoctor');
});

// Hospital Route
Route::get('/hospital-login', 'Auth\HospitalLoginController@showlogin')->name('hospital.login');
Route::post('/hospital-login', 'Auth\HospitalLoginController@login');
Route::get('/hospital/register', 'HospitalController@showRegister');
Route::post('/hospital/register', 'HospitalController@hospitalRegister');

//Admin Route
Route::middleware('auth:admin')->group(function(){
  Route::get('/admin-dashboard', 'AdminController@adminDashboard');
});
Route::get('/admin-login', 'AdminController@adminLogin');

//Socialite Route
Route::get('login/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\AuthController@handleProviderCallback');
