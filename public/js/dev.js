$(document).ready(function(){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#datepicker').datepicker();
  $('#datepicker1').datepicker().on('changeDate', function (ev) {
    $('#datepicker1').change();
  });
  $('#datepicker1').change(function () {
    $('.schedule-list').empty();
    var dt = $('#datepicker1').val();
    var id = $(this).data('ids');
    $.ajax({
      url: '/getdate',
      type: 'POST',
      data: {"date": dt,"doc_id": id},
      success: function(res){
        if(res == 'No Schedule for this day'){
          var html = "";
          html += '<div class="col-md-12">';
          html += '<span>No Schedule for this day</span>';
          html += '</div>';
          $('.schedule-list').append(html);
        }else{
          $.each(res , function(index, v) {
            // console.log(v.id);
            if (v.status == 0) {
              var html = "";
              html += '<div class="button col-md-6">';
              html += '<a class="thm-btn bgclr-1" href="'+APP_URL+"/view-appointment?&schedule_date="+v.schedule_date+"&schedule_time="+v.schedule_time+"&slug="+v.hash_url+'">'+v.schedule_time+'</a>';
              html += '</div>';
            }else if(v.status == 2){
              var html = "";
              html += '<div class="button col-md-6">'
              html += '<a class="thm-btn bgclr-1 btn-appoint disable" href="#">'+v.schedule_time+'<i style="color:green;" class="fa fa-check" aria-hidden="true"></i></a>';
              html += '</div>';
            }else{
              var html = "";
              html += '<div class="button col-md-6">';
              html += '<a class="thm-btn bgclr-1 btn-appoint disable" href="#">'+v.schedule_time+'</a>';
              html += '</div>';
            }
            // $('.schedule-list').empty();
            $('.schedule-list').append(html);
          });
        }
      }
    });
  });
  $('.done').on('click', function(){
    var id = $(this).data('ids');
    $.confirm({
      title: 'Alert!',
      content: 'Are you sure you want to mark this as done?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-green',
          action: function(){
            $.ajax({
              url: '/doctor/update/'+id,
              type: 'PUT',
              data: {"id": id},
              success: function(res){
                location.reload();
              }
            });


          }
        },
        close: function () {
        }
      }
    });
  });
  $('.done-walkin').on('click', function(){
    var id = $(this).data('ids');
    $.confirm({
      title: 'Alert!',
      content: 'Are you sure you want to mark this as done?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-green',
          action: function(){
            $.ajax({
              url: '/doctor/update_walkin/'+id,
              type: 'PUT',
              data: {"id": id},
              success: function(res){
                location.reload();
              }
            });


          }
        },
        close: function () {
        }
      }
    });
  });

  $('.resched').on('click',function(){
    var id = $(this).data('ids');
    $.confirm({
      title: 'Alert!',
      content: 'Are you sure you want to reschedule?',
      type: 'orange',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-warning',
          action: function(){
            $.ajax({
              url: '/doctor/update_re/'+id,
              type: 'PUT',
              data: {"id": id},
              success: function(res){
                location.reload();
              }
            });

          }
        },
        close: function () {
        }
      }
    });
  });
  $('.resched-walkin').on('click',function(){
    var id = $(this).data('ids');
    $.confirm({
      title: 'Alert!',
      content: 'Are you sure you want to reschedule?',
      type: 'orange',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-warning',
          action: function(){
            $.ajax({
              url: '/doctor/update_re_walkin/'+id,
              type: 'PUT',
              data: {"id": id},
              success: function(res){
                location.reload();
              }
            });

          }
        },
        close: function () {
        }
      }
    });
  });
  $('body').on('click', '.btn-appoint', function(e) {
    var date = $(this).data('date');
    var time = $(this).data('time');
    var doc_id = $(this).data('docid');
    var sched_id = $(this).data('schedid');
    if ($(this).hasClass('disable')) {
      e.preventDefault();
    }else{
      $.confirm({
        title: 'Appointment Alert!',
        content: 'Are you sure you want to appoint this date <strong>'+date+' @ '+time+'</strong>',
        type: 'blue',
        typeAnimated: true,
        buttons: {
          tryAgain: {
            text: 'GO',
            btnClass: 'btn-blue',
            action: function(){
              $.ajax({
                url: '/appointment',
                type: 'POST',
                data: {"date": date, "time": time, 'doc_id': doc_id, 'sched_id': sched_id },
                success: function(res){
                  console.log(res);
                  // if (res.msg == "Login First!") {
                  //   window.location.replace("http://appointmed-git.local/account");
                  // }
                  //else{
                  window.location.replace(APP_URL+"/appointment-list/");
                  // }
                }
              });
            }
          },
          close: function () {

          }
        }
      });
    }

  });

  $('.append').on('click',function(e){
    e.preventDefault();
    var html = "";
    html += '<div class="single-box">',
    html += '<div class="form-group">',
    html += '<div class="row removal">';
    html += '<div class = "col-md-11">';
    html += '<select class="form-control" name="healthcard[]" placeholder="Health Card" required>',
    html += '<option value="">Health Card</option>',
    html += '<option value="ac">Asalus Corporation</option>',
    html += '<option value="ac">Asalus Corporation</option>',
    html += '<option value="ahsi">Asiancare Health SYstems, lnc.</option>',
    html += '<option value="amci">Avega Managed Care, lnc.</option>',
    html += '<option value="cplsii">Carehealth Plus Systems lnternational, lnc.</option>',
    html += '<option value="chsi">Carewell Health Systems, lnc.</option>',
    html += '<option value="chsi">Caritas Health Shield, lnc.</option>',
    html += '<option value="chmf">Cooperative Health Management Federation</option>',
    html += '<option value="dcc">Dynamic Care CorP.</option>',
    html += '<option value="ehi">Eastwest Healthcare, lnc.</option>',
    html += '<option value="fmi">Fortune Medicare, lnc.</option>',
    html += '<option value="ghsi">Getwell Health SYstems, lnc.</option>',
    html += '<option value="hcdcp">Health Care & Development Corporationof the PhiliPPines</option>',
    html += '<option value="hmi">Health Maintenance, lnc.</option>',
    html += '<option value="imswci">IMS Wealth Care, lnc.</option>',
    html += '<option value="ihci">lnsular Health Care, lnc.</option>',
    html += '<option value="kihi">Kaiser lnternational Healthgroup, lnc.</option>',
    html += '<option value="lhi">Life & Health HMP, lnc.</option>',
    html += '<option value="mhci">Mazan Health Care, lnc.</option>',
    html += '<option value="mhc">Maxicare Healthcare CorP.</option>',
    html += '<option value="mhsi">Medocare Health SYstems, lnc.</option>',
    html += '<option value="mpi">Medicard Philippines, lnc.</option>',
    html += '<option value="mpi">MedicarePlus, lnc.</option>',
    html += '<option value="mhsi">Metrocare Health System, lnc.</option>',
    html += '<option value="omhsi">Optimum Medical and Healthcare Services, lnc.</option>',
    html += '<option value="pchci">Pacific Cross Health Care, lnc.</option>',
    html += '<option value="pci">PhilhealthCare, lnc.</option>',
    html += '<option value="shsi">Stotsenberg Healthcare Sytem lnc.</option>',
    html += '<option value="tmdci">Transnational Medical & Diagnostic Center,lnc.</option>',
    html += '<option value="vchsi">Value Care Health SYstems, lnc.</option>',
    html += '</select>',
    html += '</div>';
    html += '<div class="col-md-1" style="font-size: 20px; padding-left: 0px; padding-top: 2px;"><a href="#"><i class="fa fa-minus-square" aria-hidden="true"></i></a>';
    html += '</div>';
    html += '</div>',
    html += '</div>',
    $("#append").append(html);


  });
  $(document).on('click','.fa-minus-square',function(){
    // alert();
    $('.single-box').first().remove();
  });
  $('.author-box').on('click',function(){
    location.replace($(this).data('url'));
  });
  // $('.btn-appoint').on('click',function(){
  //   var date = $(this).data('date');
  //   var time = $(this).data('time');
  //   var doc_id = $(this).data('docid');
  //   var sched_id = $(this).data('schedid');
  // $.confirm({
  //   title: 'Appointment Alert!',
  //   content: 'Are you sure you want to appoint this date <strong>'+date+' @ '+time+'</strong>',
  //   type: 'blue',
  //   typeAnimated: true,
  //   buttons: {
  //     tryAgain: {
  //       text: 'GO',
  //       btnClass: 'btn-blue',
  //       action: function(){
  //         $.ajax({
  //           url: '/appointment',
  //           type: 'POST',
  //           data: {"date": date, "time": time, 'doc_id': doc_id, 'sched_id': sched_id },
  //           success: function(res){
  //             console.log(res);
  //           }
  //         });
  //       }
  //     },
  //     close: function () {
  //
  //     }
  //   }
  // });
  // });
  $('.test-remove').on('click',function(){
    $.confirm({
      title: 'Remove Alert!',
      content: 'Are you sure you want to remove this schedule?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-red',
          action: function(){
            $('#test').closest('tr').remove();
          }
        },
        close: function () {

        }
      }
    });
  });
  $('.btn-save').one('mouseover',function(){
    $.confirm({
      title: 'Alert!',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
      type: 'red',
      typeAnimated: true,
      buttons: {
        tryAgain: {
          text: 'GO',
          btnClass: 'btn-red',
          action: function(){
          }
        },
      }
    });
  });
});
